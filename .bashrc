#
# .bashrc - setup command file for the Bourne Again Shell.
#

if [ "$machtype" = "" ]; then
	export machtype=`uname -m -s | sed -e 's, ,-,'`
	export hostname=`uname -n | sed 's/\..*//'`
	export qualname=`uname -n`
fi

export SHELL=$BASH

# Allow group write access.
umask 002

apath=/fee

case $machtype in
	4.3bsd-9000/[78]*)
		xpath=/hpux/usr/bin/X11:/usr/new/X11
		mpath=hp800
		;;

	4.3bsd-9000/[34]*)
		xpath=/usr/new/X11
		mpath=hp300
		;;

	HP-UX-9000/[78]*)
		xpath=/usr/X11/bin
		mpath=hp9000s800
		;;

	IRIX64-IP21)
		xpath=/usr/bin/X11
		mpath=sgi
		;;

	FreeBSD-i386)
		xpath=/usr/X11R6/bin
		mpath=i386
		;;

	NetBSD-arm32)
		xpath=/usr/X11R6/bin
		mpath=arm32
		apath=/usr/pkg/bin
		;;

	*)
		xpath=/usr/new/X11
		mpath=foo
		apath=foo
		;;
esac

# Set all the soft limits to the hard limits.
for limit in c d m s t f p n; do
	ulimit -S$limit `ulimit -H$limit` 2>/dev/null
done

PATH=~/bin/$mpath:~/bin:~/bin/gitbin:/bin:/sbin:/etc:/usr/etc:/usr/ucb:\
/usr/bin:/usr/sbin:$xpath:/usr/site/bin:/usr/new:/usr/local/bin:\
/usr/local/sbin:/usr/local:/usr/new/mh:/usr/gnu/bin:\
/usr/local/gnu/bin:.

# Only do this stuff for interactive shells.
if [ "$PS1" != "" ];
then
	auto_resume=1
	command_oriented_history=1
	history_control=ignoredups
	ignoreeof=10
	notify=1
	unset HISTFILE
	HISTSIZE=100
	LOCALPROMPT=""

	if [ -e ~/.lprompt ]; then
	    LOCALPROMPT=`cat ~/.lprompt`
	    LOCALPROMPT=".${LOCALPROMPT}"
	fi

	# Simple Prompt.
	PS1="{\!} ${hostname}${LOCALPROMPT}$ "

	# Set terminal characteristics.
	stty intr  susp  kill 

	# Helper function to allow using csh setenv in files.
	function setenv
	{
		if [ $# -eq 0 ]; then
			export
		else
			var="$1"
			shift
			export $var="$*"
		fi
	}

	#
	# alias does not require an = sign in csh.
	#
	function alias
	{
		if [ $# -eq 0 ]; then
			builtin alias
		elif [ $# -eq 1 ]; then
			builtin alias $1
		else
			builtin alias "$1"="$2"
		fi
	}

	#
	# So, now I can share these two files with csh by using the
	# above two functions.
	#
	source ~/.aliases
	source ~/.env

	if [ -e ~/.localinit ]; then
		source ~/.localinit
	fi

	# Things that must be functions in bash
	function c
	{
		fg $1
	}
	function cd
	{
		builtin cd $1 ; dirs=`dirs` ; dirs
#		setxtitle
	}
	function po
	{
		popd ; dirs=`dirs`
#		setxtitle
	}
	function pu
	{
		pushd $1 ; dirs=`dirs`
#		setxtitle
	}
	function setxtitle
	{
		if [ "$BASH_VERSINFO" != "" -a "$EMACS" = "" ];
		then
			echo -n "]0;${qualname}:${DIRSTACK[0]}";
		fi;
	}
	alias rehash 'hash -r'

	# Machine specific aliases.
	case $machtype in
		4.3bsd-9000/[78]* | \
		4.3bsd-9000/[34]*)
			alias df	'df -t ufs'
			alias mso	'source ~/.mso'
			setenv EDITOR jove
			;;

		HP-UX-9000/[78]*)
			;;

		FreeBSD-i386)
			setenv EDITOR jove
			;;

		*)
		    	setenv EDITOR vi
			;;
	esac
fi

