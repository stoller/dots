#
# C Shell login file.
#

#--------------------------------------------------------------------------- 
# Terminal set-up.
#
alias termset 'set noglob; eval `tset -Q -s -e\ -k\ -m "switch=19200:vt100" -m "dialup:?vt100" -m "switch:?vt100" -m "network<=2400:?vt100" -m "network=19200:vt100" -m "network:?vt100" \!*`; set glob'

if ( $?TERM ) then
	termset $TERM
else
	termset
endif

switch ("$TERM")
case xterm:
	breaksw

case vt220:
	setenv TERMCAP :li#30:tc=vt100:
	stty erase 
	breaksw

case vt200:
case vt52:
case t10:
	stty erase 
	breaksw

case vt100:
case xterm-256color:
	stty erase 
	reset
	breaksw

endsw
echo Terminal type is $TERM

#--------------------------------------------------------------------------- 
# See who is here.
#
echo "-- Current users: " ; finger

#--------------------------------------------------------------------------- 
# What kind of machine,
#
echo ""
echo "-- Machine info: " ; uname -a

#--------------------------------------------------------------------------- 
# Set my HOME env variable to match the actual NFS /home/css mountpoint.
# This causes emacs to print ~/ instead of /a/home/css/stoller.
# Comment out the next line if you do not already have something that
# gets the hostname.
#
# setenv hostname `uname -n`
#
if ( "$HOME" == "/home/css/stoller" ) then
	if ( "$hostname" == "fast" ) then
		setenv HOME /remote/users/staff/stoller
	else
		setenv HOME /a/home/css/stoller
	endif
endif

#--------------------------------------------------------------------------- 
# Execute miscellaneous commands:
#
mesg y



