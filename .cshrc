#
# .cshrc - setup command file for the C Shell.
#

if ( ! $?machtype ) then
	setenv machtype `uname -m -s | sed -e 's, ,-,'`
        setenv hostname `uname -n | sed 's/\..*//'`
        setenv qualname `uname -n`
endif

# Allow group write access.
umask 002

set apath=/fee

switch ("$machtype")
	case 4.3bsd-9000/[78]* :
		set xpath=(/hpux/usr/bin/X11 /usr/new/X11)
		set mpath=hp800
		breaksw

	case 4.3bsd-9000/[34]* :
		set xpath=/usr/new/X11
		set mpath=hp300
		breaksw

	case HP-UX-9000/[78]* :
		set xpath=/usr/X11/bin
		set mpath=hp9000s800
		set user=$LOGNAME
		breaksw

	case FreeBSD-i386 :
		set xpath=/usr/X11R6/bin
		set mpath=i386
		breaksw

	case NetBSD-arm32 :
		set xpath=/usr/X11R6/bin
		set mpath=arm32
		set apath=/usr/pkg/bin
		breaksw

	default:
		set xpath=/usr/new/X11
		set mpath=foo
		breaksw
endsw

set path = ( ~/bin/${mpath} ~/bin /bin /sbin /etc /usr/etc \
		/usr/ucb /usr/bin /usr/sbin ${xpath} /usr/site/bin /usr/new \
		/usr/local/bin /usr/local/sbin /usr/local /usr/new/mh \
		/usr/gnu/bin /usr/local/gnu/bin $apath .)

if ( $?prompt ) then
	#
	# Use bash if it exists, and USE_BASH is non-zero.
	#
	if ( ! $?SHLVL ) then
		set SHLVL = 1
	endif

	if ( $?BASH == 0 && $SHLVL == 1 && -e ~/.bashrc ) then
		if ( -e ~/bin/${mpath}/bash || \
			-e /usr/gnu/bin/bash || \
			-e /usr/local/gnu/bin/bash || \
			-e /bin/bash || \
			-e /usr/local/bin/bash ) then
			exec bash -login
		endif
	endif

	if ( ! $?login ) then
		set login	= $home
	    endif

	set history=45
	set filec
	set showdotfiles
	set ignoreeof
	set notify
	set mail=(60 /usr/spool/mail/$user)

	# Simple Prompt.
	set prompt = "{\!} $hostname> "

	# Set terminal characteristics.
	stty intr  susp  kill 

	source ~/.env
	source ~/.aliases

	# More aliases that .bashrc cannot deal with.
	alias c	  '%\!*'
	alias cd  'cd \!* ; set dirs = `dirs` ; dirs '
	alias o	  'popd ; set dirs = `dirs`'
	alias p	  'pushd \!* ; set dirs = `dirs`'

	# Machine specific aliases.
	switch ("$machtype")
		case 4.3bsd-9000/[78]* :
			alias df 'df -t ufs'
			breaksw

		case 4.3bsd-9000/[34]* :
			alias df 'df -t ufs'
			breaksw

		case HP-UX-9000/[78]* :
			breaksw

		default:
			breaksw
	endsw

	# Set the editor used by "mail", etc.
	if (-e /usr/local/mg) then
	    setenv EDITOR  /usr/local/mg
	else if (-e /usr/local/bin/mg) then
	    setenv EDITOR  /usr/local/bin/mg
	else
	    setenv EDITOR  /usr/bin/vi
	endif
endif

