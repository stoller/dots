This buffer is for notes you don't want to save, and for Lisp evaluation.
If you want to create a file, visit that file with C-x C-f,
then enter the text in that file's own buffer.

(autoload 'vm "vm" "Start VM on your primary inbox." t)
(setq load-path (cons (expand-file-name "~/gnu/vm-6.75") load-path))
(setq load-path (cons (expand-file-name "~/gnu/w3") load-path))
