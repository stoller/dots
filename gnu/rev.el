;;; I like my slides to be numbered in reverse order. I am sure Tex could
;;; do this for me, but it could take me a week to figure out how. This
;;; took about 10 minutes to do.

(defun reverse-slide-file ()
  "Reverse a slides file, writing a new file called rev.tex. You must be
visiting (and in the buffer) of the slides file you want to reverse."
  (interactive)
  (save-excursion
    (let ((curbuf (current-buffer))
	  (newbuf (find-file-noselect "rev.tex")))
      (set-buffer newbuf)
      (beginning-of-buffer)
      ;; Zap the contents of the reversed file.
      (let ((beg (point)))
	(end-of-buffer)
	(delete-region beg (point)))
      (set-buffer curbuf)
      (beginning-of-buffer)
      (while (search-forward "begin{slide" (point-max) t)
	(beginning-of-line)
	(let ((beg (point)))
	  (if (null (search-forward "end{slide" (point-max) t))
	    (error "Could not find end of slide"))
	  (beginning-of-line)
	  (next-line 1)
	  (copy-region-as-kill beg (point))
	  (set-buffer newbuf)
	  (yank)
	  (exchange-point-and-mark)
	  (open-line 1)
	  (set-buffer curbuf)
	  (setq kill-ring-yank-pointer (cdr kill-ring-yank-pointer))))
      (set-buffer newbuf)
      (save-buffer)
      (kill-buffer newbuf)
      (set-buffer curbuf))))
