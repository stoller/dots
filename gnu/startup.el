;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; File:         startup.el
; Description:  Various customizations and extensions for GNU.
; Author:       Leigh Stoller
; Created:      26-Oct-87
;
; (c) Copyright 1987-2022, University of Utah, all rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq load-path (cons (expand-file-name "~/gnu")             load-path))
(setq load-path (cons (expand-file-name "~/gnu/php-mode")    load-path))
;(setq load-path (cons (expand-file-name "~/gnu/magit")       load-path))
;(setq load-path (cons (expand-file-name "~/gnu/magit-section")  load-path))
;(setq load-path (cons (expand-file-name "~/gnu/elib")       load-path))
;(setq load-path (cons (expand-file-name "~/gnu/tramp")      load-path))
;(setq load-path (cons (expand-file-name "~/gnu/egg")        load-path))

(setq Info-default-directory-list
      (append (list
	       (expand-file-name "~/gnu/vm")
	       (expand-file-name "~/gnu/bbdb/texinfo")
	       "/usr/site/info")
	      Info-default-directory-list))
(setq Info-directory-list Info-default-directory-list)

;; Find out what version of emacs we are running.
(defvar go-multi        nil)

(cond
 ((= emacs-major-version 18)
  (setq load-path (cons (expand-file-name "~/gnu/v18") load-path)))
 ((= emacs-major-version 19)
  (setq load-path (cons (expand-file-name "~/gnu/v19") load-path))
  (setq load-path (cons (expand-file-name "~/gnu/v19/mailcrypt") load-path)))
 ((or (= emacs-major-version 20)
      (= emacs-major-version 21)
      (= emacs-major-version 22)
      (= emacs-major-version 23)
      (= emacs-major-version 24)
      (= emacs-major-version 25)
      (= emacs-major-version 26)
      (= emacs-major-version 27))
  (setq load-path (cons (expand-file-name "~/gnu/v20") load-path))
  (setq load-path (cons (expand-file-name "~/gnu/v20/mailcrypt") load-path))))

;; Do appropriate window system initializations.
(cond ((and (getenv "DISPLAY")
	    window-system)
       (cond ((= emacs-major-version 18)
	      (load "v18-support"))
	     ((= emacs-major-version 19)
	      (load "v19-support"))
	     ((or (= emacs-major-version 20)
		  (= emacs-major-version 21)
		  (= emacs-major-version 22)
		  (= emacs-major-version 23)
		  (= emacs-major-version 24)
		  (= emacs-major-version 25)
		  (= emacs-major-version 26)
		  (= emacs-major-version 27))
	      (load "v20-support")))
       ;; wheel mouse
       (global-set-key [mouse-4] 'scroll-down)
       (global-set-key [mouse-5] 'scroll-up))
      (t
       (require 'mwheel)
       (require 'mouse)
       (xterm-mouse-mode t)
       (mouse-wheel-mode t)
       (menu-bar-mode   -1)
       (global-set-key [mouse-4] 'previous-line)
       (global-set-key [mouse-5] 'next-line)))

;; Global to control whether I use font lock mode.
(setq global-font-lock-mode nil)
(setq jit-lock-stealth-load 500)
(setq jit-lock-defer-time 0)
(defun null-font-lock-mode ())
(defun no-font-lock-mode ()
  (interactive)
  (setq use-font-lock-mode nil)
  (fset 'font-lock-mode 'null-font-lock-mode))

;; Helper to increase font.
(defun bigfont ()
  (interactive)
  (set-frame-font "10x20"))

;; My X server at home cannot handle matching parens properly. Too slow.
;;(if (equal (getenv "XDISPLAY_TYPE") "PCX")
;;  (setq blink-matching-paren nil))

; Some key assignments to make life easier.

(global-set-key "\C-ce"		'compile)
(global-set-key "\C-c-" 	'what-line)
(global-set-key "\C-\\" 	'set-mark-command)
(global-set-key (kbd "C-'") 	'set-mark-command)
(global-set-key "\C-c\\" 	'set-mark-command)
(global-set-key "\C-cz" 	'enlarge-window)
(global-set-key "\C-c("		'backward-sexp)
(global-set-key "\C-c)" 	'forward-sexp)
(global-set-key "\er" 		'reindent-lisp-comment)
(global-set-key "\C-x\C-b"	'electric-buffer-list)
(global-set-key "\C-cc"		'electric-command-history)
(global-set-key "\C-x\C-n"	'goto-line)
(define-key     esc-map [backspace]  'backward-kill-word)

;; I hate narrow to region/page.
(global-unset-key "\C-xnp")
(global-unset-key "\C-xnn")

;; This is not done?
(autoload 'electric-command-history "echistory" "" t nil)

;; Do not want to destroy links
(setq backup-by-copying-when-linked t)

;; Paranoia!
(setq auto-save-interval 200)

;; Do not add newlines to end of buffer.
(setq next-line-add-newlines nil)

;; Set the backspace key to work correctly no matter what. Not the same as
;; doing a global-set-key.
(load "term/bobcat")

;;; The next few things are for NMODE compatability.  Anybody remember NMODE.

;; I like the tab key as the Meta prfix. A Double tab will be a normal
;; tab unless I am in Lisp-mode, in which case it is lisp-indent. Makes
;; perfect sense to me.

(if (>= emacs-major-version 19)
  (progn
    (or key-translation-map (setq key-translation-map (make-sparse-keymap)))

    (defun translate-tab (prompt)
      "")

    (defun translate-escape (prompt)
      "	")

    (define-key key-translation-map ""  'translate-escape)
    (define-key key-translation-map "	" 'translate-tab)
    )
  (progn
    (aset keyboard-translate-table ?\^i ?\033)  ; Rebind the tab to escape
    (aset keyboard-translate-table ?\033 ?\^i)  ; Rebind the esc to tab
    ))

(global-set-key "\e\e" 'tab-to-tab-stop)    

;; The return key should do a mode sensitive indent, when appropriate.
(global-set-key "\C-m" 'newline-and-indent)

;; Nowhere left to put this but esc-e.
(global-set-key "\ee" 'eval-expression)
(put 'eval-expression 'disabled nil)

;; I like the NMODE style key binding for dabbrevs. 
(global-set-key "\e " 'dabbrev-expand)
;; Make sure case of the replacement is preserved.
(setq dabbrev-case-replace nil)

;; When I log in from home, I use a vt100. Sometimes a vt52. Remap the
;;  the backspace key back to what it should be.

;(setq TERM (getenv "TERM"))
;(cond ((or (equal TERM "vt52")
;	   (equal TERM "vt100"))
;       (aset keyboard-translate-table ?\177 ?\^?)
;       (aset keyboard-translate-table ?\^? ?\177)))

;; I like '-', '.', '*', and '_' to be considered parts of words so
;; dabbrevs can expand them correctly, and so filename-expansion skips
;; over them. This does present a problem with *, but so be it.

(modify-syntax-entry ?- "w   " lisp-mode-syntax-table)
(modify-syntax-entry ?* "w   " lisp-mode-syntax-table)
(modify-syntax-entry ?_ "w   " lisp-mode-syntax-table)

(modify-syntax-entry ?- "w   " emacs-lisp-mode-syntax-table)
(modify-syntax-entry ?* "w   " emacs-lisp-mode-syntax-table)
(modify-syntax-entry ?_ "w   " emacs-lisp-mode-syntax-table)

;;(modify-syntax-entry ?- "w   " c-mode-syntax-table)
;;(modify-syntax-entry ?. "w   " c-mode-syntax-table)
;;(modify-syntax-entry ?* "w   " c-mode-syntax-table)
(if (boundp 'c-mode-syntax-table)
    (progn
      (modify-syntax-entry ?_ "w   " c-mode-syntax-table)))

(modify-syntax-entry ?- "w   " text-mode-syntax-table)
(modify-syntax-entry ?. "w   " text-mode-syntax-table)
(modify-syntax-entry ?* "w   " text-mode-syntax-table)
(modify-syntax-entry ?_ "w   " text-mode-syntax-table)

(modify-syntax-entry ?- "w   " (standard-syntax-table))
(modify-syntax-entry ?. "w   " (standard-syntax-table))
(modify-syntax-entry ?* "w   " (standard-syntax-table))
(modify-syntax-entry ?_ "w   " (standard-syntax-table))

;; This switches tab and space so space will be the one that completes as
;; far as possible, which is the one we usually want. Stolen from Tim
;; Mueller's .emacs file.

(define-key minibuffer-local-must-match-map "\040" 'minibuffer-complete)
(define-key minibuffer-local-must-match-map "\011" 'minibuffer-complete-word)
(define-key minibuffer-local-completion-map "\040" 'minibuffer-complete)
(define-key minibuffer-local-completion-map "\011" 'minibuffer-complete-word)

(define-key minibuffer-local-filename-completion-map "\040" 'minibuffer-complete
)
(define-key minibuffer-local-filename-completion-map "\011" 'minibuffer-complete)
(define-key minibuffer-local-must-match-filename-map "\040" 'minibuffer-complete-word)
(define-key minibuffer-local-must-match-filename-map "\011" 'minibuffer-complete-word)

(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(defun my-web-mode-hook ()
  (interactive)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-attr-indent-offset nil)
  (web-mode-set-engine "underscore"))
(add-hook 'web-mode-hook  'my-web-mode-hook)

;;; Various extensions for modes. More support in site-init.el.

;;(load "mycmode")
;;(setq *author* "Leigh Stoller")
;;(setq *copyright* "University of Utah, all rights reserved.")

(setq use-cperl-mode nil)

(load "cc-mode")
(load "column-marker")

(defun my-lang-mode-hook ()
  (interactive)
  (auto-fill-mode 0)
  (set-fill-column 75)
  (column-marker-3 80))

(defun my-perl-mode-hook ()
  (interactive)
  (if use-cperl-mode
    (progn
      (setq cperl-continued-statement-offset 4)
      (setq cperl-invalid-face nil)
      (setq cperl-auto-newline nil)
      (setq cperl-indent-level 4))))

(if use-cperl-mode
  (progn
    (load "elisp-cperl-mode")
    (defalias 'perl-mode 'cperl-mode)
    (add-hook 'cperl-mode-hook 'turn-on-font-lock)
    (add-hook 'cperl-mode-hook 'my-lang-mode-hook)
    (add-hook 'cperl-mode-hook 'my-perl-mode-hook))
  (progn
    (load "perl-mode")
    (add-hook 'perl-mode-hook 'turn-on-font-lock)
    (add-hook 'perl-mode-hook 'my-lang-mode-hook)
    (add-hook 'perl-mode-hook 'my-perl-mode-hook)))

(add-hook 'tcl-mode-hook   'my-lang-mode-hook)

(load "php-mode")
(defun my-php-mode-hook ()
  (interactive)
  (setq tab-width 4)
  (php-enable-default-coding-style)
  (turn-on-font-lock)
  (global-set-key "\e " 'dabbrev-expand)
  (my-lang-mode-hook))

;;(add-hook 'php-mode-hook #'(lambda () (setq c-basic-offset 4)))
(add-hook 'php-mode-hook 'my-php-mode-hook)

(defun my-c-mode-hook ()
  (interactive)
  (my-lang-mode-hook)
  (c-set-style "bsd"))

(add-hook 'c++-mode-hook 'my-c-mode-hook)
(add-hook 'c-mode-hook 'my-c-mode-hook)

; Python stuff
(load "python")
(setq auto-mode-alist
      (cons '("\\.py$" . python-mode) auto-mode-alist))
(setq interpreter-mode-alist
      (cons '("python" . python-mode)
            interpreter-mode-alist))
(autoload 'python-mode "python-mode" "Python editing mode." t)

(add-hook 'python-mode-hook 'turn-on-font-lock)
(add-hook 'python-mode-hook 'my-lang-mode-hook)

;;; Tex hook.

(defun my-tex-hook ()
  (interactive)
  (set-fill-column 75)
  (auto-fill-mode 1)
  (modify-syntax-entry ?_ "w   " tex-mode-syntax-table)
  (setq paragraph-separate "^[ \t]*\\($\\|[\f%]\\|\\\\.*[]}][ \t]*$\\)"))

(setq latex-mode-hook 'my-tex-hook)
(setq tex-mode-hook 'my-tex-hook)

(autoload 'reverse-slide-file "rev" "Reverse slitex file" t)

;;; Mail mode stuff.

(autoload 'maybe-do-vm  "mail-support" "" t)
(autoload 'vm-mail      "mail-support" "" t)

(global-set-key "\C-cr" 'maybe-do-vm)
(global-set-key "\C-xm" 'vm-mail)

;;; I'm too used to typing "make".

(defun make ()
  (interactive)
  (compile "make"))

(defun gmake ()
  (interactive)
  (compile "gmake"))

(put 'narrow-to-page 'disabled nil)

;;; Lisp related stuff. This uses the new stuff from Eric Eide.

;; Autoloads to get the lisp shell stuff.
(autoload 'run-ucl	"lispshell" "Run an inferior Utah Common Lisp." t)
(autoload 'run-uclcomp	"lispshell" "Run an inferior Utah Common Lisp." t)
(autoload 'run-psl 	"lispshell" "Run an inferior Portable Standard Lisp." t)

;; Don't follow the output, follow the cursor.
(setq lispshell-follow-output nil)

;; Turn off returns in the output buffer.
(setq lispshell-return-sends-input nil)

;; Don't popup a window when we get any output.
(setq lispshell-popup-for-output nil)

;; Don't copy old commands to the end of the output.
(setq lispshell-copy-old-input nil)

;; Do we prompt for a file name or just start up uclcomp
(setq uclcomp-prompt-for-file nil)

;; What is the max time limit for a ucl process (in seconds).
(setq ucl-cputime-limit 3600)

;; And some specific lisp funtions.

(defun uclcomp ()
  (interactive)
  (run-uclcomp "/u/ucl/bin/uclcomp"))

(defun ucl ()
  (interactive)
  (run-ucl "/u/ucl/bin/ucl"))

;;; Mach kernel debugging.

(autoload 'machkgdb "kgdb" "" t)

(autoload 'cmushell 	"cmushell" 	"Run an inferior shell process." t)

;; Eric's fancy fill adapt mode.

(defun my-text-mode-hook ()
  (interactive)
  (filladapt-mode)
  (set-fill-column 75)
  (column-marker-3 80)
  (turn-on-auto-fill)
  (flyspell-mode 1))
(setq text-mode-hook nil)
(add-hook 'text-mode-hook 'my-text-mode-hook)

;;; Something to stick my .sig in
(defun insert-sig ()
  (interactive)
  (end-of-buffer)
  (insert-file "~/.sig"))

(global-set-key "\C-cs" 'insert-sig)

(set-fill-column 73)

(setq enable-local-variables t)

;; yet another ttt-hack.

(defun find-file-at-point (arg)
  "Find file from the area surrounding the point.
A prefix arg (e.g.^U) opens the file in another window."
  (interactive "P")
  (save-excursion
    (let ((filename))
      (re-search-backward "[^-/a-zA-Z_0-9\\.~=]")
      (forward-char 1)
      (re-search-forward "\\([-/a-zA-Z_0-9\\.~=]*\\)")

      (setq filename (buffer-substring (match-beginning 1) (match-end 1)))

      (if (file-exists-p filename)
	  (if arg
	      (find-file-other-window filename)
	    (find-file filename))
	(error "Couldn't find file named \"%s\"." filename)))))

(global-set-key "\C-x\C-p" 'find-file-at-point)

;; Shell mode changes:
(setq shell-popd-regexp  "po"
      shell-pushd-regexp "pu")

;; Man
(defun mmode ()
  (interactive)
  (Man-set-fonts))

;; Compare Windows
(defun compare ()
  (interactive)
  (compare-windows nil))

;; Squish copyright years to range. Thanks Eric! But must load it first.
(load "copyright")
(setq copyright-year-ranges t)

;; copyright-update no longer respects copyright-year-ranges
(defun my-copyright-update ()
  (interactive)
  (copyright-update))

(add-hook 'write-file-hooks 'my-copyright-update)

;; This is nice ...
(global-set-key "\C-x\C-u" 'browse-url-at-point)

;; I WANT MY BACKUP FILES HIDDEN!
(defun make-backup-file-name (file)
  (concat ".~" (file-name-nondirectory file)))

(defun backup-file-name-p (file)
  (string-match "^.~" file))

;; Convert a text url to a real URL for Mom.

(defun convert-url-to-tag ()
  (interactive)
  (let ((bounds (bounds-of-thing-at-point 'url))
	(btag   "<A HREF=\"")
	(atag   "\">Click Me</A>"))
    (print bounds)
    (save-excursion
      (goto-char (car bounds))
      (insert btag)
      (goto-char (+ (cdr bounds) (length btag)))
      (insert atag))))

;; This is nice ...
(global-set-key "\C-xh" 'convert-url-to-tag)

;;; Indent region over by a tab stop

(defun myindent ()
  (interactive)
  (if (null (mark))
    (error "Must set mark.")
    (save-excursion
      (if (> (point) (mark))
	(exchange-point-and-mark))
      (save-restriction
	(narrow-to-region (point) (mark))
	(replace-regexp "^" "    ")))))

;(setq browse-url-netscape-program "/usr/local/bin/opera")
;(setq vm-netscape-program "/usr/X11R6/bin/opera")

(setq browse-url-netscape-program "~/bin/firefox-openurl")
(setq vm-netscape-program "~/bin/firefox-openurl")

;(setq browse-url-netscape-program "netscape")
;(setq vm-netscape-program "netscape")

; Auto decompress files!
(load "jka-compr")

; and encryption of files
(load "crypt")
(setq crypt-encryption-type 'des)

;(load "tramp")

;; Calendar
(setq calendar-latitude 44.6)
(setq calendar-longitude -123.3)
(setq calendar-location-name "Corvallis, Or")
(setq islamic-holidays nil)

; Turn off highlighting in shell mode.
(setq comint-highlight-input nil)
(setq comint-highlight-prompt nil)

; Auto decompress files!
(load "jka-compr")

(defun geni-buffers ()
  (interactive)
  (shell)
  (set-buffer (get-buffer "*shell*"))
  (rename-buffer "geni-cm")
  (shell)
  (set-buffer (get-buffer "*shell*"))
  (rename-buffer "geni-sa")
  (shell)
  (set-buffer (get-buffer "*shell*"))
  (rename-buffer "geni-ch")
  (shell)
  (set-buffer (get-buffer "*shell*"))
  (rename-buffer "tbdb")
  (shell))

(defun my-log-edit-mode-hook ()
  (interactive)
  (font-lock-mode))

(add-hook 'log-edit-mode-hook 'my-log-edit-mode-hook)

; I do this all the time!
(defun my-revert-buffer ()
  (interactive)
  (if (buffer-modified-p)
      (error "Buffer has been modified!"))
  (revert-buffer nil t))

(global-set-key "\C-x\C-r"	'my-revert-buffer)
(global-set-key "\C-x\C-t"	'auto-revert-tail-mode)

; Put git commit into 

;
; From cperl-mode.el ... 
;
(defun perldoc (word)
  "Run `perldoc' on WORD."
  (interactive
   (list (let* ((default-entry (cperl-word-at-point))
                (input (read-string
                        (format "perldoc entry%s: "
                                (if (string= default-entry "")
                                    ""
                                  (format " (default %s)" default-entry))))))
           (if (string= input "")
               (if (string= default-entry "")
                   (error "No perldoc args given")
                 default-entry)
             input))))
  (require 'man)
  (let* ((case-fold-search nil)
	 (is-func (and
		   (string-match "^[a-z]+$" word)
		   (string-match (concat "^" word "\\>")
				 (documentation-property
				  'cperl-short-docs
				  'variable-documentation))))
	 (Man-switches "")
	 (manual-program (if is-func "perldoc -f" "perldoc")))
    (cond
     ((featurep 'xemacs)
      (let ((Manual-program "perldoc")
	    (Manual-switches (if is-func (list "-f"))))
	(manual-entry word)))
     (t
      (Man-getpage-in-background word)))))

(defun xml-format ()
  (interactive)
  (save-excursion
    (shell-command-on-region (point-min) (point-max)
			     "xmllint --format -" (buffer-name) t)))

;; Magit stuff
(global-set-key "\C-xm"	'magit-status)

(defun magit-submodule-update-all ()
  (interactive)
  (magit-with-toplevel
    (magit-run-git-async "submodule" "update" "--recursive" "--remote")))

(defun my-magit-mode-hook ()
  (interactive)
  (setq git-commit-nonempty-second-line nil)
  (setq git-commit-summary-max-length 1000)
  (setq git-commit-finish-query-functions '())
  (setq magit-auto-revert-mode nil)
  (transient-define-suffix magit-submodule-update ()
    :class 'magit--git-submodule-suffix
    :description "Update         git submodule update [--force] [--no-fetch]
                     [--remote] [--recursive] [--checkout|--rebase|--merge]"
    (interactive
     (magit-submodule-update-all))))

(add-hook 'magit-mode-hook 'my-magit-mode-hook)

;; autorevert stuff.
(setq auto-revert-verbose nil)

; Start the server for remote editing.
; Always last in case another one is running.
(server-start)

;; Kill unused buffers.
(require 'midnight)
(midnight-delay-set 'midnight-delay "4:30am")

(require 'desktop)

(defun sy-save-shell-buffer (desktop-dirname)
  ;; we only need to save the current working directory
  default-directory)

(defun sy-create-shell-buffer (_file-name buffer-name misc)
  "MISC is the value returned by `sy-save-shell-buffer'.
_FILE-NAME is nil."
  (let ((default-directory misc))
    ;; create a shell buffer named BUFFER-NAME in directory MISC
    (shell buffer-name)))

;; save all shell-mode buffers
(add-hook 'shell-mode-hook
	  (lambda () (setq-local desktop-save-buffer #'sy-save-shell-buffer)))
;; restore all shell-mode buffers
(add-to-list 'desktop-buffer-mode-handlers
	     '(shell-mode . sy-create-shell-buffer))

(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)

(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(autoload 'gfm-mode "markdown-mode"
   "Major mode for editing GitHub Flavored Markdown files" t)
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))

;; Switch to man window
(setq Man-notify-method 'aggressive)

