;; Do X11 specific window initializations.

;; No multi-window support under v18.
(defvar go-multi nil)

;; Have to load this for Gnu to work in X.
(load "term/x-win")

;; Get proper mouse support.
(load "x-mouse")

;; Make mouse button keys work like xterm cut/paste.
(define-key mouse-map x-button-s-left 'x-cut-mark-front)
(define-key mouse-map x-button-s-left-up 'x-cut-text)
(define-key mouse-map x-button-s-middle 'x-paste-text)
(define-key mouse-map x-button-s-right 'x-mouse-fill-to-top)

;; Make the line center to the top.
(defun x-mouse-fill-to-top (arg)
  (if (x-mouse-set-point arg)
    (recenter 0)))

(defvar x-mouse-old-point nil)

;; On the mouse down, move the point to the mouse pointer, but remember
;; where we came from"
(defun x-cut-mark-front (arg)
  (setq x-mouse-old-point (point))
  (x-mouse-set-point arg))

;; Cut the text between the current location (which was marked by the
;; down event), then return back to the original place when finished.
(defun x-cut-text (arg &optional kill)
  (if (coordinates-in-window-p arg (selected-window))
      (let ((opoint (point))
            beg end)
           (x-mouse-set-point arg)
           (message "got point")
           (setq beg (min opoint (point))
                 end (max opoint (point)))
           (x-store-cut-buffer (buffer-substring beg end))
           (copy-region-as-kill beg end)
           (if kill (delete-region beg end))
           (goto-char x-mouse-old-point))
    (message "Mouse not in selected window")))

(x-set-cursor-color "Red")
(x-set-mouse-color "Red")
(x-set-border-color "white")


