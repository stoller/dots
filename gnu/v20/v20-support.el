;; I do not like menu bars or scroll bars on my editor.
(menu-bar-mode   -1)
(scroll-bar-mode -1)
(tool-bar-mode   -1)
(transient-mark-mode -1)

;; I have this bound in the window manager, but at home I do not use a
;; window manager, and I hate that stupid menu.
(global-unset-key [C-down-mouse-1])

;; Same goes for this one.
(global-unset-key [C-down-mouse-3])

;; Robert.
(set-face-foreground 'highlight "black")
(set-face-foreground 'region "black")

;; I DO NOT WANT EMACS CHECKING OUT FILES FOR ME!
(remove-hook 'find-file-not-found-hooks 'vc-file-not-found-hook)
(remove-hook 'find-file-hooks 'vc-find-file-hook)
(define-key global-map "\C-x\C-q" 'toggle-read-only)

(setq initial-frame-alist
      (cons (cons 'name
		  (concat "E-" (system-name)))
	    initial-frame-alist))


