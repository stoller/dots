;ELC   
;;; Compiled by stoller@golden.lbs.intnet.net on Mon Nov 30 11:38:44 1998
;;; from file /home/stoller/src/mailcrypt-3.5.1/expect.el
;;; in Emacs version 20.3.1
;;; with bytecomp version 2.50
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.
(if (and (boundp 'emacs-version)
	 (< (aref emacs-version (1- (length emacs-version))) ?A)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`expect.el' was compiled for Emacs 19.29 or later"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\207" [require cl timer] 2)
#@174 *If non-nil, report how much data has arrived in the process buffer.
This variable is buffer-local to all Expect buffers, and should be set
inside @code{with-expect} forms.
(defvar expect-message nil (#$ . -681))
#@256 If a number, start the Expect searches from that point.
If not, start searches from `(point-min)'.
This variable is typically `let' to t before calling `with-expect'
when waiting for output from a process that is already started and may
have output data.
(defvar expect-start nil (#$ . 901))
#@79 The number of seconds to wait before an Expect timeout element is triggered.

(defvar expect-timeout 10 (#$ . 1199))
(byte-code "\305\300!\204 \306\300	B\305\302!\204 \306\302	B\305\303!\204  \306\303	B\305\304!\204, \306\304	B\306\207" [expect-processes current-load-list expect-asynchronous expect-process expect-current-info boundp nil] 2)
(defalias 'expect-make-info #[(process message point) "	\n\303\211\257\207" [process message point nil] 5])
(defalias 'expect-info-process '(macro . #[(info) "\301\302E\207" [info nth 0] 3]))
(defalias 'expect-info-message '(macro . #[(info) "\301\302E\207" [info nth 1] 3]))
(defalias 'expect-info-point '(macro . #[(info) "\301\302E\207" [info nth 2] 3]))
(defalias 'expect-info-set-point '(macro . #[(info point) "\302\303\304E	E\207" [info point setcar nthcdr 2] 4]))
(defalias 'expect-info-sentinels '(macro . #[(info) "\301\302E\207" [info nth 3] 3]))
(defalias 'expect-info-set-sentinels '(macro . #[(info sentinels) "\302\303\304E	E\207" [info sentinels setcar nthcdr 3] 4]))
(defalias 'expect-info-timer '(macro . #[(info) "\301\302E\207" [info nth 4] 3]))
(defalias 'expect-info-set-timer '(macro . #[(info timer) "\302\303\304E	E\207" [info timer setcar nthcdr 4] 4]))
(defalias 'expect-info-queries '(macro . #[(info) "\301\302E\207" [info nthcdr 5] 3]))
(defalias 'expect-info-set-queries '(macro . #[(info queries) "\302\303\304E	E\207" [info queries setcdr nthcdr 4] 4]))
(defalias 'expect-find-info '(macro . #[(process) "\301\302BB\207" [process assoc (expect-processes)] 3]))
#@628 Set things up for communication with PROGRAM.
FORMS will be evaluated in the normal manner.  To talk to the process,
use `expect' and `expect-send'.  See the manual for full documentation.
This macro returns nil.

If PROGRAM is a string, start that program.  If PROGRAM is a list, use
the first element of that list as the program and the remainder as the
parameters.  If PROGRAM is a process, talk to that process.

PROGRAM will be started up in a new, fresh temporary buffer.  The
buffer will be killed upon completion.  If PROGRAM is a process,
a new buffer won't be created, and the buffer won't be killed upon
completion.
(defalias 'with-expect '(macro . #[(program &rest forms) "\304\305!\304\306!\307\310	\311B\312B\313BB\314	D\315\316\317\320\nDE\321BB\322D\323\324\"*BBBBBD\207" [point buf program forms make-symbol "buf" "point" save-excursion let ((generate-new-buffer " *expect*")) ((point)) (expect-process expect-current-info) set-buffer unless setq expect-process expect-start-process ((error "Can't start program")) expect-setup append ((unless (expect-info-sentinels expect-current-info) (expect t)) nil)] 9 (#$ . 2766)]))
(defalias 'expect-start-process #[(program) ";\203 \301\302p#\207:\203 \303\301\302p@A%\207\304!\203\" \207\305\306!\207" [program start-process "expect" apply processp error "Illegal process spec"] 6])
#@245 Set things up for asynchronous communication with PROGRAM.
This macro behaves like `with-expect', only that `expect' calls
contained in FORMS will be evaluated asyncronously.

See the documentation of the `with-expect' macro for documentation.
(defalias 'with-expect-asynchronous '(macro . #[(program &rest forms) "\302\303\304	BBE\207" [program forms let ((expect-asynchronous t)) with-expect] 5 (#$ . 4130)]))
#@55 Execute FORMS when REGEXP  has arrived in the buffer.
(defalias 'expect '(macro . #[(regexp &rest forms) "\302\303\304\305	BBDE\207" [regexp forms expect-1 function lambda nil] 6 (#$ . 4549)]))
#@289 Try each clause until one succeeds.
Each clause looks like (CONDITION BODY).  CONDITION should be
a regular expression to wait for, or a process status symbol.
If CONDITION is satisfied (i. e., the data has arrived or
the process has entered the specified status), BODY will be executed.
(defalias 'expect-cond '(macro . #[(&rest clauses) "\302	\2031 	@@;\203 	@@\202 \303	@@DB\304\305\302	@ABBDC@B	\211A\210\202 \306\237)B\207" [result clauses nil quote function lambda expect-1] 4 (#$ . 4751)]))
#@44 Execute FORMS when the process has exited.
(defalias 'expect-exit '(macro . #[(&rest forms) "\301\302\303\304BBDD\207" [forms expect-exit-1 function lambda nil] 5 (#$ . 5265)]))
#@46 Send STRING to the current buffer's process.
(defalias 'expect-send '(macro . #[(string) "\301\302E\207" [string process-send-string expect-process] 3 (#$ . 5450)]))
#@46 Initialize Expect data, filter and sentinel.
(defalias 'expect-setup #[(&optional point) "\306	\n\206\f \206\f e#\211B\307\310\"\210\311\312\"\210\313!q\207" [expect-process expect-message point expect-start expect-current-info expect-processes expect-make-info set-process-filter expect-filter set-process-sentinel expect-sentinel process-buffer] 5 (#$ . 5623)])
#@39 Remove Expect infestation of PROCESS.
(defalias 'expect-shutdown #[(process) "\302\303	\"	\"\304\305\"\210\306\305\"\207" [process expect-processes delq assoc set-process-filter nil set-process-sentinel] 4 (#$ . 6001)])
#@30 Kill PROCESS and its buffer.
(defalias 'expect-kill #[(process) "\302!\303	!\203 \304	!\210\305!\210\306!)\207" [process buffer process-buffer buffer-name kill-buffer expect-shutdown delete-process] 2 (#$ . 6231)])
#@64 Wait until the current outstanding command has been performed.
(defalias 'expect-wait #[nil "\303	\"\304\n!\210\305\303	\"\233@\203# \306!\307>\203# \310\311\"\210\202	 \312\n!\210)\313\207" [expect-process expect-processes info assoc expect-setup-timer 5 process-status (open run) accept-process-output 1 expect-cancel-timer nil] 4 (#$ . 6457)])
(defalias 'expect-1 #[(&rest clauses) "\306\211\211\204 \307\310!\210\f\2036 \f@\300=\203% \fA@\fAA\211\202 \f\211A@\f\211A@D	B\202 \203D \311\233\306E\240\210	\237C\244\210\f?\205R \312 +\207" [timeout entries entry expect-process clauses expect-current-info nil error "No expect in this buffer" 4 expect-wait expect-timeout expect-asynchronous] 5])
(defalias 'expect-exit-1 #[(function) "\204 \304\305!\210\306	\"\307\n\233\307\n8C\244\240\210)\310\207" [expect-process expect-processes info function error "No expect in this buffer" assoc 3 nil] 3])
#@54 Controlling Expect function run as a process filter.
(defalias 'expect-filter #[(process string) "p\305\216\306\307!q\210`\310!U\212\310!b\210\fc\210\310!`\306\223\210\311!\210)\312!\313>\2058 \2058 \310!b,\207" [process expect-process old-buffer moving string ((byte-code "\301!\203	 q\210\301\207" [old-buffer buffer-name] 2)) nil process-buffer process-mark expect-find-event process-status (open run)] 3 (#$ . 7392)])
#@30 Controlling Expect sentinel.
(defalias 'expect-sentinel #[(process status) "\304!\305>\203\f \306!\210\304!\307=\205F \212\310!\205E \311\310!!\205E \310!q\210\312\313\n\"8\203A \212\211A@ \210)\2021 \314!)*\207" [process expect-process expect-processes sentinels process-status (open run) expect-find-event exit process-buffer buffer-name 3 assoc expect-shutdown] 4 (#$ . 7835)])
#@36 Find (and execute) the next event.
(defalias 'expect-find-event #[(process) "\306	\"\307\n8\310\n\233\211@\311\312\n!\210\nA@\203\" \313\314d\"\210\205\243 @@\315=\2034 \316!\202\243 dV\205\243 b\210\205\243 @@\211;\203U \317@@\311\315#\202q \315=\204t \320>\203l \321!=\202q \322\323\"\203\233 \324\n!\210\nAA`\240\210\325\n\233\fA\241\210\212@A@ \210)\311\321!\326>\203\233 \327!\210A\211\204A \311-\207" [process expect-processes info point queries clause assoc 2 5 nil expect-setup-timer message "Expect received %d bytes" t expect-kill re-search-forward (exit run stop signal open closed) process-status error "Illegal condition: %s" expect-cancel-timer 4 (open run) expect-find-event cond] 5 (#$ . 8236)])
(defalias 'expect-setup-timer #[(info) "\3028\211\205 \303!\210	\304	A@\305	AA@#\240)\207" [info timer 4 expect-cancel-timer run-at-time nil] 6])
(defalias 'expect-cancel-timer #[(info) "\3018@\205 \302\303\304\217\207" [info 4 #1=#:G68000 (byte-code "\301\3028@!\207" [info cancel-timer 4] 3) ((error))] 3])
(byte-code "\300\301\302\303#\210\300\301\304\305#\210\300\306\302\307#\210\300\306\304\310#\210\300\311\302\303#\210\300\311\304\312#\210\300\313\302\303#\210\300\313\304\314#\210\315\301!\207" [put expect lisp-indent-function 1 edebug-form-spec (form body) expect-exit 0 (body) with-expect (form body) with-expect-asynchronous (form body) provide] 4)
