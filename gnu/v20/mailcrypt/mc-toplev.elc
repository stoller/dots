;ELC   
;;; Compiled by stoller@golden.lbs.intnet.net on Mon Nov 30 12:46:59 1998
;;; from file /home/stoller/gnu/mailcrypt/mc-toplev.el
;;; in Emacs version 20.3.1
;;; with bytecomp version 2.50
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.
(if (and (boundp 'emacs-version)
	 (< (aref emacs-version (1- (length emacs-version))) ?A)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`mc-toplev.el' was compiled for Emacs 19.29 or later"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\303\304\305\217\210\306\307\310\303\311$\210\306\312\313\303\311$\210\306\314\315\303\311$\207" [require mailcrypt mail-utils nil (byte-code "\300\301!\207" [require mailalias] 2) ((error)) autoload mc-scheme-pgp "mc-pgp" t mc-scheme-pgp50 "mc-pgp5" mc-scheme-gpg "mc-gpg"] 5)
(defalias 'mc-cleanup-recipient-headers #[(str) "\301\302\303\304\"\"\207" [str mapcar mc-strip-address mc-split "\\([ 	\n]*,[ 	\n]*\\)+"] 5])
(defalias 'mc-find-headers-end #[nil "\212eb\210\301\302\303!\304Q!\210\305\306!\203 \301\307\310\311#\210\305\312!\203% \301\307\310\311#\210\313 )\207" [mail-header-separator re-search-forward "^" regexp-quote "\n" looking-at "^::\n" "^\n" nil t "^##\n" point-marker] 4])
#@225 *Encrypt the current buffer.

Exact behavior depends on current major mode.

With \[universal-argument], prompt for User ID to sign as.

With \[universal-argument] \[universal-argument], prompt for encryption scheme to use.
(defalias 'mc-encrypt #[(arg) "\301\302\211#\207" [arg mc-encrypt-region nil] 4 (#$ . -1355) "p"])
#@30 *Encrypt the current region.
(defalias 'mc-encrypt-region #[(arg start end) "	\236\243\306\n\236\243\206 \307\310\310\310\311Y\203# \312\313!\314\315Y\2035 \316\317\320\"\"A\310\f&-\207" [major-mode mc-modes-alist mode-alist func sign scheme encrypt mc-encrypt-generic nil 4 read-string "User ID: " t 16 assoc completing-read "Encryption Scheme: " from arg mc-schemes start end] 7 (#$ . -1685) "p\nr"])
#@48 *Generic function to encrypt a region of data.
(defalias 'mc-encrypt-generic #[(&optional recipients scheme start end from sign) "\212\204 \306 \307!\204 \310!	\204 \311 \307	!\204# \310	!\312\313!\210\n;\2034 \314\315\n\"\202E \n\204A \316\317\320!!\202E \321\322!\210\204K \f\323\324 \"A\n	%\205` \312\325!\210\326)\207" [start end recipients scheme mc-default-scheme from point-min-marker markerp copy-marker point-max-marker run-hooks mc-pre-encryption-hook mc-split "\\([ 	\n]*,[ 	\n]*\\)+" mc-cleanup-recipient-headers read-string "Recipients: " error "mc-encrypt-generic: recipients not string or nil" assoc encryption-func mc-post-encryption-hook t sign] 6 (#$ . -2123)])
#@204 *Encrypt a message for RECIPIENTS using the given encryption SCHEME.
RECIPIENTS is a comma separated string. If SCHEME is nil, use the value
of `mc-default-scheme'.  Returns t on success, nil otherwise.
(defalias 'mc-encrypt-message #[(&optional recipients scheme start end from sign) "\212\306 \307\214eb\210\310\311\312\n!\313Q!\210e`}\210\314\315!\203, \314\316!\204, \203, \317ed\"\210\320\321\322\323\324!\"!)\f\204P \214eb\210\310\311\312\n!\325Q!\210`	}\210\326\327!)\204b \203] \202a \330\331\"\204j 	\204s \332 \333\f &+\207" [default-recipients headers-end mail-header-separator mail-aliases from recipients mc-find-headers-end nil re-search-forward "^" regexp-quote "$" featurep mailalias mail-abbrevs expand-mail-aliases mc-strip-addresses mapcar cdr mc-get-fields "to\\|cc\\|bcc" "\n" mail-fetch-field "From" read-from-minibuffer "Recipients: " point-max-marker mc-encrypt-generic mc-use-default-recipients start end scheme sign] 7 (#$ . -2829)])
#@90 *Decrypt a message in the current buffer.

Exact behavior depends on current major mode.
(defalias 'mc-decrypt #[nil "	\236\243\304\n\236\243\206 \305\211 *\207" [major-mode mc-modes-alist mode-alist func decrypt mc-decrypt-message] 3 (#$ . -3823) nil])
#@186 Decrypt whatever message is in the current buffer.
Returns a pair (SUCCEEDED . VERIFIED) where SUCCEEDED is t if the encryption
succeeded and VERIFIED is t if it had a valid signature.
(defalias 'mc-decrypt-message #[nil "\212\306		\203 \307\310\311	 \"A\310\312	 \"A\"\204C \f\203C \f@A\211\203C \307\310\311\n \"A\310\312\n \"A\"\211\204C \fA\202 \205e \313\314!\210\310\315\n \"A@A\"eb\210@\203c \313\316!\210),\207" [mc-schemes mc-default-scheme scheme limits schemes resultval nil mc-message-delimiter-positions assoc msg-begin-line msg-end-line run-hooks mc-pre-decryption-hook decryption-func mc-post-decryption-hook] 5 (#$ . 4088)])
#@262 *Sign a message in the current buffer.

Exact behavior depends on current major mode.

With one prefix arg, prompts for private key to use, with two prefix args,
also prompts for encryption scheme to use.  With negative prefix arg,
inhibits clearsigning (pgp).
(defalias 'mc-sign #[(arg) "\301\302\211#\207" [arg mc-sign-region nil] 4 (#$ . -4753) "p"])
#@27 *Sign the current region.
(defalias 'mc-sign-region #[(arg start end) "	\236\243\306\n\236\243\206 \307\310\310\311Y\203$ \312\313\314\"\"A\315Y\203/ \316\317!\f\320W%,\207" [major-mode mc-modes-alist mode-alist func from scheme sign mc-sign-generic nil 16 assoc completing-read "Encryption Scheme: " 4 read-string "User ID: " 0 arg mc-schemes start end] 7 (#$ . -5114) "p\nr"])
(defalias 'mc-sign-generic #[(withkey scheme start end unclearsig) "\204 	\n\204 \306 \307\n!\204 \310\n!\204 \311 \307!\204( \310!\312\313!\210\314\315 \"A\n\f$\205? \312\316!\210\317\207" [scheme mc-default-scheme start end withkey unclearsig point-min-marker markerp copy-marker point-max-marker run-hooks mc-pre-signature-hook assoc signing-func mc-post-signature-hook t] 5])
#@25 Clear sign the message.
(defalias 'mc-sign-message #[(&optional withkey scheme start end unclearsig) "\212\306 	\204 eb\210\307\310\311\n!\312Q!\210\214`}\210\313\314!)\204$ \f\204+ \315 \316	\f%*\207" [headers-end withkey mail-header-separator start end scheme mc-find-headers-end re-search-forward "^" regexp-quote "\n" mail-fetch-field "From" point-max-marker mc-sign-generic unclearsig] 6 (#$ . 5919)])
#@89 *Verify a message in the current buffer.

Exact behavior depends on current major mode.
(defalias 'mc-verify #[nil "	\236\243\304\n\236\243\206 \305\211 *\207" [major-mode mc-modes-alist mode-alist func verify mc-verify-signature] 3 (#$ . -6344) nil])
#@156 *Verify the signature of the signed message in the current buffer.
Show the result as a message in the minibuffer. Returns t if the signature
is verified.
(defalias 'mc-verify-signature #[nil "\212\305		\203 \306\307\310	 \"A\307\311	 \"A\"\204C \f\203C \f@A\211\203C \306\307\310\n \"A\307\311\n \"A\"\211\204C \fA\202 \204M \312\313!\202X \307\314\n \"A@A\",\207" [mc-schemes mc-default-scheme scheme limits schemes nil mc-message-delimiter-positions assoc signed-begin-line signed-end-line error "Found no signed message in this buffer." verification-func] 5 (#$ . -6607)])
#@137 *Insert your public key at point.
With one prefix arg, prompts for user id to use. With two prefix
args, prompts for encryption scheme.
(defalias 'mc-insert-public-key #[(&optional userid scheme) "\204 	\n\204 \303\304 \"An\204 \305c\210\303\306 \"A\n!\207" [scheme mc-default-scheme userid assoc user-id "\n" key-insertion-func] 3 (#$ . -7205) (byte-code "\303	<\203, 	@\247\203, 	@\304Y\203 \305\306\307\n\"\n\"AB	@\310Y\203, \311\312!B)\207" [arglist current-prefix-arg mc-schemes nil 16 assoc completing-read "Encryption Scheme: " 4 read-string "User ID: "] 4)])
#@100 *Add all public keys in the buffer to your keyring.

Exact behavior depends on current major mode.
(defalias 'mc-snarf #[nil "	\236\243\304\n\236\243\206 \305\211 *\207" [major-mode mc-modes-alist mode-alist func snarf mc-snarf-keys] 3 (#$ . -7794) nil])
#@53 *Add all public keys in the buffer to your keyring.
(defalias 'mc-snarf-keys #[nil "e\306\307	\212\310\311\215\210\312\313\314\f\211\315=\203 \316\202 \317#!.\207" [mc-schemes mc-default-scheme scheme limits found start 0 nil done (byte-code "\203 \306\307\310 \"A\307\311 \"A	#\n\204> \203> @A\211\203> \306\307\310\f \"A\307\311\f \"A	#\211\204> A\202 \n\204J \312\313\"\210\202  \nA\307\314\f \"A\n@\nA\"\\\202  " [mc-default-scheme start limits schemes scheme found mc-message-delimiter-positions assoc key-begin-line key-end-line throw done snarf-func] 5) message format "%d new key%s found" 1 "" "s" schemes] 6 (#$ . -8059) nil])
#@47 *Verify the signature in the current message.
(defalias 'mc-rmail-summary-verify-signature #[nil "\302=\204\n \303\304!\210\212	q\210\305 )\207" [major-mode rmail-buffer rmail-summary-mode error "mc-rmail-summary-verify-signature called in inappropriate buffer" mc-verify] 2 (#$ . -8727) nil])
#@39 *Decrypt the contents of this message
(defalias 'mc-rmail-summary-decrypt-message #[nil "\302=\204\n \303\304!\210\212	q\210\305 )\207" [major-mode rmail-buffer rmail-summary-mode error "mc-rmail-summary-decrypt-message called in inappropriate buffer" mc-decrypt] 2 (#$ . -9028) nil])
#@52 *Adds keys from current message to public key ring
(defalias 'mc-rmail-summary-snarf-keys #[nil "\302=\204\n \303\304!\210\212	q\210\305 )\207" [major-mode rmail-buffer rmail-summary-mode error "mc-rmail-summary-snarf-keys called in inappropriate buffer" mc-snarf] 2 (#$ . -9320) nil])
#@47 *Verify the signature in the current message.
(defalias 'mc-rmail-verify-signature #[nil "\301\232\204\n \302\303!\210\304\305!\210\306\305!\210\307 \205 \304\305!\207" [mode-name "RMAIL" error "mc-rmail-verify-signature called in a non-RMAIL buffer" rmail-add-label "verified" rmail-kill-label mc-verify-signature] 2 (#$ . -9613) nil])
#@39 *Decrypt the contents of this message
(defalias 'mc-rmail-decrypt-message #[nil "\305	\306\232\204\f \307\310!\210\311\216\312 \210\313 \211@\204 \314 \202x \n\315=\204D \n\204. \316\317!\203D \320 \210\321\322!\210\323\324!\210A\205x \323\325!\202x \326\327!\330ed#\210\314 \210\331\332\"\210eb\210\333\f\334\335 \336\261\210\337 \210\340 \210\341\342\343 !!\210\344\345\346\"\210\347\305!)*\207" [decryption-result mode-name mc-always-replace tmp mc-version nil "RMAIL" error "mc-rmail-decrypt-message called in a non-RMAIL buffer" ((byte-code "\301=\203	 \302 \210\301\207" [major-mode rmail-edit-mode rmail-abort-edit] 2)) rmail-edit-current-message mc-decrypt-message rmail-abort-edit never y-or-n-p "Replace encrypted message with decrypted? " rmail-cease-edit rmail-kill-label "edited" rmail-add-label "decrypted" "verified" generate-new-buffer "*Mailcrypt Viewing*" copy-to-buffer switch-to-buffer t "From Mailcrypt-" " " current-time-string "\n" rmail-convert-file rmail-mode use-local-map copy-keymap current-local-map local-set-key "q" mc-rmail-view-quit set-buffer-modified-p] 6 (#$ . -9958) nil])
(defalias 'mc-rmail-view-quit #[nil "p\301\302!\210\303 \210\304!)\207" [buf set-buffer-modified-p nil rmail-quit kill-buffer] 2 nil nil])
#@49 *Verify the signature in the current VM message
(defalias 'mc-vm-verify-signature #[nil "t\203 \300 \210\301 \210\302 \210\303 \210\214\304 \210\305 )\207" [vm-follow-summary-cursor vm-select-folder-buffer vm-check-for-killed-summary vm-error-if-folder-empty vm-widen-page mc-verify-signature] 1 (#$ . -11224) nil])
#@49 *Decrypt the contents of the current VM message
(defalias 'mc-vm-decrypt-message #[nil "\305\211t\203 \306 \210\307 \210\310 \210\311 \210\312 \210\313 \314 \210\315\316\317\217\204- \320 \210\321\322!\202e \n\323=\204E \n\204= \324\325!\203E \326\326 )\202e \327\330!\331\fed#\210\320 \210\332\f\333\"\210eb\210c\210\334\305!\210\335\333!)*\207" [from-line vm-frame-per-edit mc-always-replace this-command tmp nil vm-follow-summary-cursor vm-select-folder-buffer vm-check-for-killed-summary vm-error-if-folder-read-only vm-error-if-folder-empty vm-leading-message-separator vm-edit-message condition-data (byte-code "\300 @\207" [mc-decrypt-message] 1) ((error (byte-code "\301 \210\302\303\304A@\"!\207" [condition-data vm-edit-message-abort error message "Decryption failed: %s"] 4))) vm-edit-message-abort error "Decryption failed." never y-or-n-p "Replace encrypted message with decrypted? " vm-edit-message-end generate-new-buffer "*Mailcrypt Viewing*" copy-to-buffer switch-to-buffer t set-buffer-modified-p vm-mode] 4 (#$ . -11547) nil])
#@63 *Snarf public key from the contents of the current VM message
(defalias 'mc-vm-snarf-keys #[nil "t\203 \300 \210\301 \210\302 \210\303 \210\214\304 \210\305 )\207" [vm-follow-summary-cursor vm-select-folder-buffer vm-check-for-killed-summary vm-error-if-folder-empty vm-widen-page mc-snarf-keys] 1 (#$ . -12607) nil])
(defalias 'mc-gnus-verify-signature #[nil "\304 \210\305 	\306\n\307\"\310\216\203 \311!\210\312!q\210\202# \313\n!\210\214~\210\314 -\207" [#1=#:GnusStartBufferWindow gnus-original-article-buffer #2=#:buf #3=#:w gnus-summary-select-article selected-window get-buffer-window visible ((select-window #1#)) select-window window-buffer pop-to-buffer mc-verify-signature] 3 nil nil])
(defalias 'mc-gnus-snarf-keys #[nil "\304 \210\305 	\306\n\307\"\310\216\203 \311!\210\312!q\210\202# \313\n!\210\214~\210\314 -\207" [#1=#:GnusStartBufferWindow gnus-original-article-buffer #2=#:buf #3=#:w gnus-summary-select-article selected-window get-buffer-window visible ((select-window #1#)) select-window window-buffer pop-to-buffer mc-snarf-keys] 3 nil nil])
(defalias 'mc-gnus-decrypt-message #[nil "\306 \210\307\310\311	\")\2044 \312 \313\f\314\"\315\216\203) \316!\210\317!q\210\202- \320\f!\210\214~\210\321 -\207\312 \313\314\"\322\216\203W \316!\210\317!q\210\202\\ \320!\210\323\324!\210\214~\210\321 @\204n \325 \202\214 \326 \204\212 \327=\204\212 \204\205 \330\331!\203\212 \332 \202\214 \325 -\207" [case-fold-search gnus-version #1=#:GnusStartBufferWindow gnus-article-buffer #2=#:buf #3=#:w gnus-summary-select-article nil string-match "Gnus" selected-window get-buffer-window visible ((select-window #1#)) select-window window-buffer pop-to-buffer mc-decrypt-message ((select-window #4=#:GnusStartBufferWindow)) gnus-summary-edit-article t gnus-summary-edit-article-postpone gnus-group-read-only-p never y-or-n-p "Replace encrypted message on disk? " gnus-summary-edit-article-done #4# #5=#:buf #6=#:w mc-always-replace] 3 nil nil])
#@66 If 0, never back up MH messages.  If 3, always back up messages.
(defvar mc-mh-backup-msg 3 (#$ . 14612))
(defalias 'mc-needs-decrypt #[nil "\212\304\211\211\203- @A\211\203- \305\306\307	 \"A\306\310	 \"A\"\211\204- A\211\204 \n,\207" [mc-schemes scheme limits schemes nil mc-message-delimiter-positions assoc msg-begin-line msg-end-line] 6])
#@68 Decrypt the contents of the current MH message in the show buffer.
(defalias 'mc-mh-decrypt-message #[nil "\306\307!\310!\311\n!\312\312\313=?\205  \206  \314\315!\211\203b \212\nq\210\316 \203T \317	!q\210\320	\307\"\210\321 @\211\203H \322!\210\202P \323\324!\210\325\312!\210\326\312!\210)\f\205\216 \327 \210\330!\202\216 \212\nq\210\316 \203\202 \321 @\211\203| eb\210\325\312!\202\215 \323\324!\202\215 \f?\205\215 \327 \210\330!)-\207" [msg msg-filename mh-show-buffer show-buffer decrypt-okay decrypt-on-disk mh-get-msg-num t mh-msg-filename get-buffer nil never y-or-n-p "Replace encrypted message on disk? " mc-needs-decrypt create-file-buffer insert-file-contents mc-decrypt-message save-buffer message "Decryption failed." set-buffer-modified-p kill-buffer mh-invalidate-show-buffer mh-show mc-always-replace mc-mh-backup-msg] 4 (#$ . 14972) "P"])
#@50 *Verify the signature in the current MH message.
(defalias 'mc-mh-verify-signature #[nil "\303 \210\304 \305	!\210\n\203 \306p!\210\307\216\310 *\207" [mh-in-show-buffer-saved-window mh-show-buffer mh-bury-show-buffer mh-show selected-window switch-to-buffer-other-window bury-buffer ((select-window mh-in-show-buffer-saved-window)) mc-verify-signature] 2 (#$ . -15855) nil])
(defalias 'mc-mh-snarf-keys #[nil "\303 \210\304 \305	!\210\n\203 \306p!\210\307\216\310 *\207" [mh-in-show-buffer-saved-window mh-show-buffer mh-bury-show-buffer mh-show selected-window switch-to-buffer-other-window bury-buffer ((select-window mh-in-show-buffer-saved-window)) mc-snarf-keys] 2 nil nil])
