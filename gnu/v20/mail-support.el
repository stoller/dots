;;; I use VM now.

(require 'vm)
(require 'mailabbrev)
(require 'bbdb)

;; BBDB stuff

(bbdb-initialize 'vm 'sendmail)
(add-hook 'mail-setup-hook 'mail-abbrevs-setup)
(add-hook 'mail-setup-hook 'bbdb-define-all-aliases)
(add-hook 'vm-mail-mode-hook 'font-lock-mode)
(setq bbdb/mail-auto-create-p nil)
(setq bbdb-always-add-addresses 'never)
(define-key vm-mail-mode-map "\ea" 'bbdb-complete-name)

;; For compilation
(or (boundp 'vm-mode-map)
    (load-library "vm-vars"))

(defun my-bbdb-update-record ()
  (interactive)
  (let ((bbdb/mail-auto-create-p t))
    (bbdb/vm-update-record)))

(add-hook 'mail-setup-hook
	  '(lambda ()
	     (substitute-key-definition 'next-line 'mail-abbrev-next-line
					mail-mode-map global-map)))

;;; Usual hook.

(defun my-mail-mode-hook ()
  (set-fill-column 75)
  (turn-on-auto-fill))

(setq mail-mode-hook 'my-mail-mode-hook)

(put 'backup-inhibited 'permanent-local t)

;;; When I compose a new message, I like the cursor to be at the first
;;; field I need to fill in.

(defun my-compose-hook (to subject cc)
  (make-local-variable 'backup-inhibited)
  (setq backup-inhibited t)
  (set-buffer-modified-p t)		; Make sure buffer is written
  (save-buffer)
  (set-file-modes buffer-file-name 384)
  (goto-char (point-min))
  (if (equal to "")
    (re-search-forward "^To: $" nil t)
    (if (equal subject "")
      (re-search-forward "^Subject: $" nil t)
      (progn
	(re-search-forward "^------" nil t)
	(next-line 1)))))

;;(setq mh-compose-letter-function 'my-compose-hook)


;;; Indent region over by a tab stop, and put a > at the beginning of each
;;; line.

(defun over ()
  (interactive)
  (if (null (mark))
    (error "Must set mark.")
    (save-excursion
      (if (> (point) (mark))
	(exchange-point-and-mark))
      (save-restriction
	(narrow-to-region (point) (mark))
	(replace-regexp "^" "> ")))))


;; Do not use rmail on any machine other than snowball.

(defun maybe-do-vm ()
  (interactive)
  (if (or (equal (system-name) "golden.ballmoss.com")
	  (equal (system-name) "C884963-A.crvlls1.or.home.com")
	  (equal (system-name) "stoller.peak.org")
	  (equal (system-name) "moab.cs.utah.edu")
	  (equal (getenv "OS") "Windows_NT"))
    (progn
      (vm))
    (error "No RMAIL here")))

(defun my-mh-yank-cur-msg ()
  (interactive)
  (let ((buffer  (current-buffer))
	(from    nil)
	(subject nil))
    (set-buffer mh-sent-from-folder)
    (set-buffer mh-show-buffer)
    (save-excursion
      (setq from    (mh-get-field "From:"))
      (setq subject (mh-get-field "Subject:")))
    ;; Emacs 19 bug. mark-active can be t even when no mark set.
    (if (not (mark t))
      (setq mark-active nil))
    (set-buffer buffer)
    (let ((to-point (point)))
      (mh-yank-cur-msg)
      (save-excursion
	(narrow-to-region to-point to-point)
	(insert "From: " from "\n")
	(insert "Subject: " subject "\n\n")
	(mh-insert-prefix-string mh-ins-buf-prefix)
	(widen)))))

;;(define-key mh-letter-mode-map "\C-c\C-y" 'my-mh-yank-cur-msg)

;;
;; Convert text tag to html tag.
;;
(defun convert-url-to-tag ()
  (interactive)
  (let ((bounds (bounds-of-thing-at-point 'url))
	(btag   "<A HREF=\"")
	(atag   "\">Click Me</A>"))
    (print bounds)
    (save-excursion
      (goto-char (car bounds))
      (insert btag)
      (goto-char (+ (cdr bounds) (length btag)))
      (insert atag))))

(global-set-key "\C-xh" 'convert-url-to-tag)

;;; Stick in a reply-to header for Renee and I.
(defun insert-broller ()
  (interactive)
  (insert "Reply-To: Leigh and Renee <broller@fast.cs.utah.edu>"))

(global-set-key "\C-xr" 'insert-broller)

;;; Stick in a reply-to for tbops
(defun insert-tbops ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (search-forward "Subject:")
    (next-line 1)
    (beginning-of-line)
    (open-line 1)
    (insert "Reply-To: Testbed Ops <testbed-ops@fast.cs.utah.edu>")))

(global-set-key "\C-xt" 'insert-tbops)
