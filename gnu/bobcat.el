;;; HP terminals usually encourage using ^H as the rubout character

(setq TERMTYPE (getenv "TERM"))

(let ((the-table (make-string 128 0)))
  (let ((i 0))
    (while (< i 128)
      (aset the-table i i)
      (setq i (1+ i))))
  ;; Swap ^H and DEL
  (cond
   ((or (equal TERMTYPE "vt100")
	(equal TERMTYPE "vt100"))
    (aset the-table ?\177 ?\^?)
    (aset the-table ?\^? ?\177))
   (t
    (aset the-table ?\177 ?\^h)
    (aset the-table ?\^h ?\177)))
  (setq keyboard-translate-table the-table))

