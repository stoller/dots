;; Function key bindings for HP 9000 Bobcat workstations. Leigh Stoller

(require 'keypad)


(setq BOBCAT-map (make-keymap))

;"The BOBCAT keymap maps the 8 function keys of the HP 9000 keyboard."

;; Set up the commnads you want bound to a particular function key.
(keypad-default "A" 'compile)
(keypad-default "B" 'lisp-send-defun)
(keypad-default "C" 'compile)
(keypad-default "D" 'compile)
(keypad-default "E" 'compile)
(keypad-default "F" 'compile)
(keypad-default "G" 'compile)
(keypad-default "H" 'compile)
(keypad-default "I" 'scroll-up)
(keypad-default "J" 'scroll-down)


;; Define what character sequences map to what function placeholder. The
;;  A,B,C... are generic names in the function-keymap. Below, you define
;;  what functions they are mapped to.
(setup-terminal-keymap BOBCAT-map
		       '(("11~" . ?A)
			 ("12~" . ?B)
			 ("13~" . ?C)
			 ("14~" . ?D)
			 ("15~" . ?E)
			 ("17~" . ?F)
			 ("18~" . ?G)
			 ("19~" . ?H)
			 ("5~"  . ?I)
			 ("6~"  . ?J)))

;; Define the prefix key for the Bobcat keymap.
(define-key global-map "[" BOBCAT-map)
(global-set-key "[" BOBCAT-map)

(define-key global-map "\C-[[" CSI-map)
(global-set-key "\C-[[" CSI-map)
