(require 'custom)

(custom-declare-face 'font-lock-comment-face
  '((((class grayscale) (background light))
     (:foreground "DimGray" :bold t :italic t))
    (((class grayscale) (background dark))
     (:foreground "LightGray" :bold t :italic t))
    (((class color) (background light)) (:foreground "wheat"))
    (((class color) (background dark)) (:foreground "wheat"))
    (t (:bold t :italic t)))
  "Font Lock mode face used to highlight comments."
  :group 'font-lock-highlighting-faces)

(custom-declare-face 'font-lock-string-face
  '((((class grayscale) (background light)) (:foreground "DimGray" :italic t))
    (((class grayscale) (background dark)) (:foreground "LightGray" :italic t))
    (((class color) (background light)) (:foreground "RosyBrown"))
    (((class color) (background dark)) (:foreground "honeydew"))
    (t (:italic t)))
  "Font Lock mode face used to highlight strings."
  :group 'font-lock-highlighting-faces)

