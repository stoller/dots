;; (load "gdb")

(setq kgdb-command-name "gdb")

(defun machkgdb ()
  (interactive)
  (kgdb "/crash/mach_kernel" nil))

(defun kgdb (path corepath)
  "Run gdb on kernel FILE and image file CORE in buffer *kgdb-FILE*.
The directory containing FILE becomes the initial working directory
and source-file directory for GDB.  If you wish to change this, use
the GDB commands `cd DIR' and `directory'."
  (interactive
   (let*
       ((bestfile (concat
		  "/sys/"
		  (upcase (substring (system-name) 0
				     (string-match "\\." (system-name))))
		  "/vmunix.gdb"))
       (deffile (or (and (file-exists-p bestfile) bestfile) "/vmunix"))
       (defcore "/dev/mem"))
     (list
      (expand-file-name
       (read-file-name
	(format "Run kernel gdb on file (default %s): " deffile) "" deffile))
      (expand-file-name
       (read-file-name
	(format "Core file (default %s): " defcore) "" defcore)))))
  (let ((file (file-name-nondirectory path)))
    (gdb (concat "gdb -k " file " " corepath))))
