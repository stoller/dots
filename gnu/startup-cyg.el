;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; File:         startup.el
; Description:  Various customizations and extensions for GNU.
; Author:       Leigh Stoller
; Created:      26-Oct-87
;
; (c) Copyright 1987, University of Utah, all rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq load-path (cons (expand-file-name "~/gnu")            load-path))
(setq load-path (cons (expand-file-name "~/gnu/vm")         load-path))
(setq load-path (cons (expand-file-name "~/gnu/bbdb/lisp")  load-path))
(setq load-path (cons (expand-file-name "~/gnu/elib")         load-path))

(setq Info-default-directory-list
      (append (list
	       (expand-file-name "~/gnu/vm")
	       (expand-file-name "~/gnu/bbdb/texinfo")
	       "/usr/site/info")
	      Info-default-directory-list))
(setq Info-directory-list Info-default-directory-list)

;; Find out what version of emacs we are running.
(defvar go-multi        nil)

(cond
 ((= emacs-major-version 18)
  (setq load-path (cons (expand-file-name "~/gnu/v18") load-path)))
 ((= emacs-major-version 19)
  (setq load-path (cons (expand-file-name "~/gnu/v19") load-path))
  (setq load-path (cons (expand-file-name "~/gnu/v19/mailcrypt") load-path)))
 ((or (= emacs-major-version 20)
      (= emacs-major-version 21))
  (setq load-path (cons (expand-file-name "~/gnu/v20") load-path))
  (setq load-path (cons (expand-file-name "~/gnu/v20/mailcrypt") load-path))))

;; Do appropriate window system initializations.
(cond ((and (getenv "DISPLAY")
	    window-system)
       (cond ((= emacs-major-version 18)
	      (load "v18-support"))
	     ((= emacs-major-version 19)
	      (load "v19-support"))
	     ((or (= emacs-major-version 20)
		  (= emacs-major-version 21))
	      (load "v20-support"))))
      (t
       (display-time)))

;; My X server at home cannot handle matching parens properly. Too slow.
;;(if (equal (getenv "XDISPLAY_TYPE") "PCX")
;;  (setq blink-matching-paren nil))

; Some key assignments to make life easier.

(global-set-key "\C-ce"		'compile)
(global-set-key "\C-c-" 	'what-line)
(global-set-key "\C-\\" 	'set-mark-command)
(global-set-key "\C-cz" 	'enlarge-window)
(global-set-key "\C-c("		'backward-sexp)
(global-set-key "\C-c)" 	'forward-sexp)
(global-set-key "\er" 		'reindent-lisp-comment)
(global-set-key "\C-x\C-b"	'electric-buffer-list)
(global-set-key "\C-cc"		'electric-command-history)
(global-set-key "\C-x\C-c"	'kill-emacs)
(global-set-key "\C-x\C-n"	'goto-line)
(define-key     esc-map [backspace]  'backward-kill-word)

;; This is not done?
(autoload 'electric-command-history "echistory" "" t nil)

;; Do not want to destroy links
(setq backup-by-copying-when-linked t)

;; Paranoia!
(setq auto-save-interval 200)

;; Do not add newlines to end of buffer.
(setq next-line-add-newlines nil)

;; Set the backspace key to work correctly no matter what. Not the same as
;; doing a global-set-key.
(load "term/bobcat")

;;; The next few things are for NMODE compatability.  Anybody remember NMODE.

;; I like the tab key as the Meta prfix. A Double tab will be a normal
;; tab unless I am in Lisp-mode, in which case it is lisp-indent. Makes
;; perfect sense to me.

(if (>= emacs-major-version 19)
  (progn
    (or key-translation-map (setq key-translation-map (make-sparse-keymap)))

    (defun translate-tab (prompt)
      "")

    (defun translate-escape (prompt)
      "	")

    (define-key key-translation-map ""  'translate-escape)
    (define-key key-translation-map "	" 'translate-tab)
    )
  (progn
    (aset keyboard-translate-table ?\^i ?\033)  ; Rebind the tab to escape
    (aset keyboard-translate-table ?\033 ?\^i)  ; Rebind the esc to tab
    ))

(global-set-key "\e\e" 'tab-to-tab-stop)    

;; The return key should do a mode sensitive indent, when appropriate.
(global-set-key "\C-m" 'newline-and-indent)

;; Nowhere left to put this but esc-e.
(global-set-key "\ee" 'eval-expression)
(put 'eval-expression 'disabled nil)

;; I like the NMODE style key binding for dabbrevs. 
(global-set-key "\e " 'dabbrev-expand)
;; Make sure case of the replacement is preserved.
(setq dabbrev-case-replace nil)

;; When I log in from home, I use a vt100. Sometimes a vt52. Remap the
;;  the backspace key back to what it should be.

;(setq TERM (getenv "TERM"))
;(cond ((or (equal TERM "vt52")
;	   (equal TERM "vt100"))
;       (aset keyboard-translate-table ?\177 ?\^?)
;       (aset keyboard-translate-table ?\^? ?\177)))

;; I like '-', '.', '*', and '_' to be considered parts of words so
;; dabbrevs can expand them correctly, and so filename-expansion skips
;; over them. This does present a problem with *, but so be it.

(modify-syntax-entry ?- "w   " lisp-mode-syntax-table)
(modify-syntax-entry ?* "w   " lisp-mode-syntax-table)
(modify-syntax-entry ?_ "w   " lisp-mode-syntax-table)

(modify-syntax-entry ?- "w   " emacs-lisp-mode-syntax-table)
(modify-syntax-entry ?* "w   " emacs-lisp-mode-syntax-table)
(modify-syntax-entry ?_ "w   " emacs-lisp-mode-syntax-table)

;;(modify-syntax-entry ?- "w   " c-mode-syntax-table)
;;(modify-syntax-entry ?. "w   " c-mode-syntax-table)
;;(modify-syntax-entry ?* "w   " c-mode-syntax-table)
(if (boundp 'c-mode-syntax-table)
    (progn
      (modify-syntax-entry ?_ "w   " c-mode-syntax-table)))

(modify-syntax-entry ?- "w   " text-mode-syntax-table)
(modify-syntax-entry ?. "w   " text-mode-syntax-table)
(modify-syntax-entry ?* "w   " text-mode-syntax-table)
(modify-syntax-entry ?_ "w   " text-mode-syntax-table)

(modify-syntax-entry ?- "w   " (standard-syntax-table))
(modify-syntax-entry ?. "w   " (standard-syntax-table))
(modify-syntax-entry ?* "w   " (standard-syntax-table))
(modify-syntax-entry ?_ "w   " (standard-syntax-table))

;; This switches tab and space so space will be the one that completes as
;; far as possible, which is the one we usually want. Stolen from Tim
;; Mueller's .emacs file.

(define-key minibuffer-local-must-match-map "\040" 'minibuffer-complete)
(define-key minibuffer-local-must-match-map "\011" 'minibuffer-complete-word)
(define-key minibuffer-local-completion-map "\040" 'minibuffer-complete)
(define-key minibuffer-local-completion-map "\011" 'minibuffer-complete-word)

;;; Various extensions for modes. More support in site-init.el.

(load "mycmode")
(setq *author* "Leigh Stoller")
(setq *copyright* "University of Utah, all rights reserved.")

;;; Tex hook.

(defun my-tex-hook ()
  (interactive)
  (set-fill-column 75)
  (auto-fill-mode 1)
  (modify-syntax-entry ?_ "w   " tex-mode-syntax-table)
  (setq paragraph-separate "^[ \t]*\\($\\|[\f%]\\|\\\\.*[]}][ \t]*$\\)"))

(setq latex-mode-hook 'my-tex-hook)
(setq tex-mode-hook 'my-tex-hook)

(autoload 'reverse-slide-file "rev" "Reverse slitex file" t)

;;; Mail mode stuff.

(autoload 'maybe-do-vm  "mail-support" "" t)
(autoload 'vm-mail      "mail-support" "" t)

(global-set-key "\C-cr" 'maybe-do-vm)
(global-set-key "\C-xm" 'vm-mail)

;;; I'm too used to typing "make".

(defun make ()
  (interactive)
  (compile "make"))

(defun gmake ()
  (interactive)
  (compile "gmake"))

(put 'narrow-to-page 'disabled nil)

;;; Lisp related stuff. This uses the new stuff from Eric Eide.

;; Autoloads to get the lisp shell stuff.
(autoload 'run-ucl	"lispshell" "Run an inferior Utah Common Lisp." t)
(autoload 'run-uclcomp	"lispshell" "Run an inferior Utah Common Lisp." t)
(autoload 'run-psl 	"lispshell" "Run an inferior Portable Standard Lisp." t)

;; Don't follow the output, follow the cursor.
(setq lispshell-follow-output nil)

;; Turn off returns in the output buffer.
(setq lispshell-return-sends-input nil)

;; Don't popup a window when we get any output.
(setq lispshell-popup-for-output nil)

;; Don't copy old commands to the end of the output.
(setq lispshell-copy-old-input nil)

;; Do we prompt for a file name or just start up uclcomp
(setq uclcomp-prompt-for-file nil)

;; What is the max time limit for a ucl process (in seconds).
(setq ucl-cputime-limit 3600)

;; And some specific lisp funtions.

(defun uclcomp ()
  (interactive)
  (run-uclcomp "/u/ucl/bin/uclcomp"))

(defun ucl ()
  (interactive)
  (run-ucl "/u/ucl/bin/ucl"))

;;; Mach kernel debugging.

(autoload 'machkgdb "kgdb" "" t)

(autoload 'cmushell 	"cmushell" 	"Run an inferior shell process." t)

;; Eric's fancy fill adapt mode.

(load "filladapt")
(add-hook 'text-mode-hook 'turn-on-auto-fill)
(cond ((< emacs-major-version 20)
       (load "fa-extras"))
      (t
       (add-hook 'text-mode-hook 'turn-on-filladapt-mode)))

;; Ispell mode.

;(load "ispell")

(autoload 'ispell-buffer "ispell" 
	  "Check spelling of every word in the buffer" t)
(autoload 'ispell-word "ispell" 
	  "Check spelling of a word" t)
(global-set-key "\e$" 'ispell-word)

(setq ispell-required-versions '("3.1.13"))

;;; Something to stick my .sig in
(defun insert-sig ()
  (interactive)
  (end-of-buffer)
  (insert-file "~/.sig"))

(global-set-key "\C-cs" 'insert-sig)

(set-fill-column 73)

(setq enable-local-variables t)

;; yet another ttt-hack.

(defun find-file-at-point (arg)
  "Find file from the area surrounding the point.
A prefix arg (e.g.^U) opens the file in another window."
  (interactive "P")
  (save-excursion
    (let ((filename))
      (re-search-backward "[^-/a-zA-Z_0-9\\.~=]")
      (forward-char 1)
      (re-search-forward "\\([-/a-zA-Z_0-9\\.~=]*\\)")

      (setq filename (buffer-substring (match-beginning 1) (match-end 1)))

      (if (file-exists-p filename)
	  (if arg
	      (find-file-other-window filename)
	    (find-file filename))
	(error "Couldn't find file named \"%s\"." filename)))))

(global-set-key "\C-x\C-p" 'find-file-at-point)

;; Shell mode changes:
(setq shell-popd-regexp  "po"
      shell-pushd-regexp "pu")

;; Man
(defun mmode ()
  (interactive)
  (Man-set-fonts))

;; Compare Windows
(defun compare ()
  (interactive)
  (compare-windows nil))

;; Roland ...
(if (>= emacs-major-version 20)
  (add-hook 'write-file-hooks 'copyright-update))

;; This is nice ...
(global-set-key "\C-x\C-u" 'browse-url-at-point)

;; I WANT MY BACKUP FILES HIDDEN!
(defun make-backup-file-name (file)
  (concat ".~" (file-name-nondirectory file)))

(defun backup-file-name-p (file)
  (string-match "^.~" file))

;; Convert a text url to a real URL for Mom.

(defun convert-url-to-tag ()
  (interactive)
  (let ((bounds (bounds-of-thing-at-point 'url))
	(btag   "<A HREF=\"")
	(atag   "\">Click Me</A>"))
    (print bounds)
    (save-excursion
      (goto-char (car bounds))
      (insert btag)
      (goto-char (+ (cdr bounds) (length btag)))
      (insert atag))))

;; This is nice ...
(global-set-key "\C-xh" 'convert-url-to-tag)

;;; Indent region over by a tab stop

(defun myindent ()
  (interactive)
  (if (null (mark))
    (error "Must set mark.")
    (save-excursion
      (if (> (point) (mark))
	(exchange-point-and-mark))
      (save-restriction
	(narrow-to-region (point) (mark))
	(replace-regexp "^" "    ")))))

;; PCL-CVS
(setq load-path (cons (expand-file-name "~/gnu/pcl-cvs") load-path))
(load-file "~/gnu/pcl-cvs/pcl-cvs-startup.el")

(setq browse-url-netscape-program "/usr/local/bin/opera")
(setq vm-netscape-program "/usr/local/bin/opera")

;(setq browse-url-netscape-program "netscape")
;(setq vm-netscape-program "netscape")

; Auto decompress files!
(load "jka-compr")

; and encryption of files
(load "crypt")
(setq crypt-encryption-type 'des)
