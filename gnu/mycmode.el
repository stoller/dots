;;; Some helpful functions for C mode.

;;; New cc-mode stuff from Robert.

(fmakunbound 	'c-mode)
(makunbound 	'c-mode-map)
(fmakunbound 	'c++-mode)
(makunbound 	'c++-mode-map)
(makunbound 	'c-style-alist)

(load "myfaces")
(load "cc-mode")

(load "perl-mode")
(add-hook 'perl-mode-hook 'turn-on-font-lock)
(add-hook 'perl-mode-hook 'turn-on-filladapt-mode)

(load "php-mode-102")
(add-hook 'php-mode-hook #'(lambda () (setq c-basic-offset 4)))
(add-hook 'php-mode-user-hook 'turn-on-font-lock)
(add-hook 'php-mode-user-hook 'turn-on-filladapt-mode)

(setq c-style-alist
      (append c-style-alist
	      '(("LBS"
		 (c-basic-offset . 8)
		 (c-comment-only-line-offset . 0)
		 (c-offsets-alist . ((statement-block-intro . +)
				     (knr-argdecl-intro . +)
				     (label . 0)
				     (statement-cont . +)))))))

;;; Kernel mode from Mike. 

(defun KERNEL-C-HOOK ()
  (interactive)
  (turn-on-filladapt-mode)
  (c-set-style "LBS"))

(defun C++-C-HOOK ()
  (interactive)
  (turn-on-filladapt-mode)
  (c++-mode)
  (c-set-style "LBS"))

(defun JAVA-HOOK ()
  (turn-on-filladapt-mode)
  (interactive))

(defun mycmode ()
  (interactive)
  (add-hook 'c-mode-hook 'KERNEL-C-HOOK)
  (c-mode)
  (c-set-style "LBS"))  

(defun myc++mode ()
  (interactive)
  (add-hook 'c++-mode-hook 'C++-C-HOOK)
  (c++-mode))

(defun myjavamode ()
  (interactive)
  (add-hook 'java-mode-hook 'JAVA-HOOK)
  (java-mode))

(defun make-c-header ()
  (interactive)
  (beginning-of-buffer)
  (insert-file "~/gnu/C-header")
  (beginning-of-buffer)
  (search-forward "File:")
  (end-of-line)
  (insert-string (file-name-nondirectory (buffer-file-name)))
  (search-forward "Author:")
  (end-of-line)
  (insert-string *author*)
  (search-forward "Date:")
  (end-of-line)
  (insert-C-date-string)
  (search-forward "(c) Copyright")
  (end-of-line)
  (insert-C-copyright-notice)
  (beginning-of-buffer)
  (search-forward "Description:")
  (end-of-line)
)

(defun insert-C-copyright-notice ()
  (let* ((time-string (current-time-string))
	 (time-string-length (length time-string)))
	(insert-string (substring time-string
				  (- time-string-length 4)
				  time-string-length) ", ")
	(insert-string *copyright*)))
  
(defun insert-C-date-string ()
  "Insert a date string of the form dd-mon-yr into the buffer at the point"
  (let* ((time-string (current-time-string))
	(time-string-length (length time-string)))
    (insert-string (substring time-string 8 10) "-")   ;; Put in the Day.
    (insert-string (substring time-string 4 7) "-")    ;; Put in the Month.
    (insert-string (substring time-string
			      (- time-string-length 2) ;; Put in the Year.
			      time-string-length))))
