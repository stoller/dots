
;;;; -*-Emacs-Lisp-*- Lisp Interpreters in Buffers
;;;; Written by Eric Eide, last modified on August 13, 1991.
;;;; (C) Copyright 1991, University of Utah and Eric Eide
;;;;
;;;; COPYRIGHT NOTICE
;;;;
;;;; This program is free software; you can redistribute it and/or modify it
;;;; under the terms of the GNU General Public License as published by the Free
;;;; Software Foundation; either version 1, or (at your option) any later
;;;; version.
;;;;
;;;; This program is distributed in the hope that it will be useful, but 
;;;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;;;; for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with this program; if not, write to the Free Software Foundation, Inc.,
;;;; 675 Mass Ave, Cambridge, MA 02139, USA.

;;;; AUTHORS
;;;;
;;;; This Lisp-in-a-Buffer mode was written by Eric Eide (eeide@cs.utah.edu)
;;;; and is based on two earlier modes: cmulisp, written by Olin Shivers, and
;;;; the University of Utah's lispshell, written by Robert Kessler and Leigh
;;;; Stoller.  Below are the copyright notices from the previous modes:
;;;;
;;;;   cmulisp:
;;;;     Copyright Olin Shivers (1988).
;;;;     Please imagine a long, tedious, legalistic 5-page gnu-style copyright
;;;;     notice appearing here to the effect that you may use this code any way
;;;;     you like, as long as you don't charge money for it, remove this
;;;;     notice, or hold me liable for its results.
;;;;   Univeristy of Utah lispshell:
;;;;     (c) Copyright 1986, University of Utah, all rights reserved.
;;;;
;;;; Addresses:
;;;;
;;;;   Eric Eide (eeide@cs.utah.edu)
;;;;   University of Utah
;;;;   3190 Merrill Engineering Building
;;;;   Salt Lake City, Utah  84112
;;;;
;;;;   Olin Shivers (shivers@cs.cmu.edu)
;;;;   Robert Kessler (kessler@cs.utah.edu)
;;;;   Leigh Stoller (stoller@cs.utah.edu)

;;;; SUMMARY
;;;;
;;;; This file defines a Lisp-in-a-Buffer mode (Lispshell mode) built on top of
;;;; comint mode.  Lispshell mode is similar to, and is intended to replace,
;;;; its counterpart in the standard GNU Emacs distribution and the University
;;;; of Utah's previous Lispshell mode.
;;;;
;;;; Since this mode is built on top of the general command-interpreter-in-a-
;;;; buffer mode (comint mode), it shares a common base functionality and a
;;;; common set of bindings with all modes derived from comint mode.  For
;;;; documentation on comint mode, and the hooks available for customizing it,
;;;; see the file comint.el.

;;;; BRIEF COMMAND DOCUMENTATION
;;;;
;;;; Comint mode commands, common to all comint-derived modes:
;;;;
;;;; Key     Command                           Description
;;;; ------- --------------------------------- --------------------------------
;;;; M-p     comint-previous-input             Cycle backward in input history
;;;; M-n     comint-next-input                 Cycle forward in input history
;;;; M-s     comint-previous-similar-input     Search input history for command
;;;;                                           with the prefix already typed
;;;; C-c r   comint-previous-input-matching    Search backward in input history
;;;;                                           for substring match
;;;; M-P     comint-msearch-input              Search backward for prompt
;;;; M-N     comint-psearch-input              Search forward for prompt
;;;; C-c R   comint-msearch-input-matching     Search backward for prompt and
;;;;                                           command prefix string
;;;;         send-invisible                    Send input without buffer echo
;;;;
;;;; C-a     comint-bol                        Beginning of line, skip prompt
;;;; C-c C-u comint-kill-input                 (^U) Kill current input
;;;; C-c C-w backward-kill-word                (^W) Kill previous word
;;;; C-c C-c comint-interrupt-subjob           (^C) Interrupt inferior process
;;;; C-c C-\ comint-quit-subjob                (^\) Quit inferior process
;;;; C-c C-z comint-stop-subjob                (^Z) Suspend inferior process
;;;;         comint-continue-subjob            Restart a suspended process
;;;;         comint-kill-subjob                Kill inferior process, no mercy
;;;;
;;;; C-c C-o comint-kill-output                Kill last batch of process
;;;;                                           output
;;;; C-c C-s comint-show-output                Move to start of the last batch
;;;;         (moved from C-c C-r)              of process output
;;;;
;;;; Lispshell mode commands:
;;;;
;;;; Key     Command                           Description
;;;; ------- --------------------------------- --------------------------------
;;;; Tab     lisp-indent-line                  Indent line as Lisp code
;;;; Delete  backward-delete-char-untabify     Delete backward, turning Tabs
;;;;                                           into spaces
;;;; C-d     lispshell-delchar-or-maybe-eof    Delete forward, or send EOF-like
;;;;                                           text to inferior Lisp
;;;;         lispshell-send-eof                Send EOF-like text to Lisp
;;;; Return  lispshell-send-input-or-maybe-    Either send input to inferior
;;;;         newline                           process, or insert newline
;;;;                                           (switch-settable option)
;;;; C-c Ret lispshell-send-input              Send input to inferior process
;;;;
;;;; C-c C-l lispshell-load-file               Tell Lisp to load a file
;;;; C-c C-k lispshell-compile-file            Tell Lisp to "kompile" a file
;;;; C-c C-d lispshell-eval-defun              Send current defun to Lisp
;;;; C-c C-e lispshell-eval-expression         Send current sexp to Lisp
;;;; C-c C-r lispshell-eval-region             Send current region to Lisp
;;;; C-c C-t lispshell-set-target-buffer       Mark current buffer as the
;;;;                                           default "target" buffer; see doc
;;;;                                           for var lispshell-target-buffer
;;;; C-c C-x lispshell-rerun                   Rerun the Lisp that previously
;;;;                                           ran in the current buffer
;;;;
;;;; C-] a   lispshell-send-debugger-abort     Tell Lisp debugger to abort
;;;; C-] b   lispshell-send-debugger-backtrace Tell Lisp debugger to show
;;;;                                           function backtrace
;;;; C-] t   lispshell-send-debugger-backtrace As above
;;;; C-] c   lispshell-send-debugger-continue  Tell Lisp debugger to continue
;;;; C-] q   lispshell-send-debugger-quit      Tell Lisp debugger to quit
;;;; C-] e   lispshell-eval-expression         Send input to inferior Lisp
;;;;
;;;; M-C-q   indent-sexp                       Indent every line of sexp
;;;; M-C-x   lispshell-eval-defun              Send current defun to Lisp
;;;; C-x C-e lispshell-eval-last-sexp          Send previous sexp to Lisp
;;;;
;;;; In addition, the following commands are added to Lisp and Scheme modes:
;;;;
;;;; Key     Command                           Description
;;;; ------- --------------------------------- --------------------------------
;;;; C-c C-t lispshell-set-target-buffer       Set or unset a specific "target"
;;;;                                           buffer for this buffer; see doc
;;;;                                           for var lispshell-target-buffer
;;;; C-c C-p lispshell-popup-target-buffer     Display Lisp process' buffer
;;;; C-c M-p lispshell-switch-to-target-buffer Display and select Lisp process'
;;;;                                           buffer
;;;;
;;;; C-c C-d lispshell-eval-defun              Send current defun to Lisp
;;;; C-c M-d lispshell-eval-defun-and-go       Send current defun to Lisp and
;;;;                                           go to Lisp process' buffer
;;;; C-c C-e lispshell-eval-expression         Send current sexp to Lisp
;;;; C-c M-e lispshell-eval-expression-and-go  Send current sexp to Lisp and go
;;;;                                           go to Lisp process' buffer
;;;; C-c C-r lispshell-eval-region             Send current region to Lisp
;;;; C-c M-r lispshell-eval-region-and-go      Send current region to Lisp and
;;;;                                           go to Lisp process' buffer
;;;; C-c C-b lispshell-eval-buffer             Send current buffer to Lisp
;;;; C-c M-b lispshell-eval-buffer-and-go      Send current buffer to Lisp and
;;;;                                           go to Lisp process' buffer
;;;; C-c C-l lispshell-load-file               Tell Lisp to load a file and go
;;;;                                           to Lisp process' buffer
;;;; C-c C-k lispshell-compile-file            Tell Lisp to "kompile" a file
;;;;                                           and go to Lisp process' buffer
;;;;
;;;; C-] a   lispshell-send-debugger-abort     Tell Lisp debugger to abort
;;;; C-] b   lispshell-send-debugger-backtrace Tell Lisp debugger to show
;;;;                                           function backtrace
;;;; C-] t   lispshell-send-debugger-backtrace As above
;;;; C-] c   lispshell-send-debugger-continue  Tell Lisp debugger to continue
;;;; C-] q   lispshell-send-debugger-quit      Tell Lisp debugger to quit
;;;; C-] e   lispshell-eval-expression         Send current sexp to Lisp
;;;; C-] p   lispshell-popup-target-buffer     Display Lisp process' buffer
;;;;
;;;; M-C-x   lispshell-eval-defun              Send current defun to Lisp
;;;; C-x C-e lispshell-eval-last-sexp          Send previous sexp to Lisp
;;;;
;;;; Commands for starting specific Lisp processes:
;;;;
;;;; run-ucl           Run Utah Common Lisp in buffer *UCL*
;;;; run-uclcomp       Run Utah Common Lisp compiler in buffer *UCL-Compiler*;
;;;;                   prompt for file to be compiled
;;;; run-class-uclcomp Run class version of Utah Common Lisp compiler in buffer
;;;;                   *UCL-Compiler*; prompt for file to be compiled
;;;; run-hpcl          Run Hewlett-Packard Common Lisp in buffer *HPCL*
;;;;
;;;; run-us            Run Utah Scheme in buffer *US*
;;;; run-uscomp        Run Utah Scheme compiler in buffer *US-Compiler*
;;;; run-cus           Run Concurrent Utah Scheme in buffer *CUS*
;;;;
;;;; run-psl           Run Portable Standard Lisp in buffer *PSL*
;;;; run-pcls          Run the Portable Common Lisp Subset in buffer *PCLS*
;;;;
;;;; run-lisp          Run a specific Lisp program; use its name to guess the
;;;;                   appropriate setup
;;;; run-remote-lisp   Run a specific Lisp program on a specific machine; use
;;;;                   its name to guess the appropriate setup

;;;; YOUR .EMACS FILE
;;;;
;;;; If lispshell.el lives in some non-standard directory, you must tell GNU
;;;; Emacs where to find it.
;;;;
;;;;   (setq load-path (cons (expand-file-name "~jones/lib/emacs") load-path))
;;;;
;;;; To autoload the lispshell commands, you may want to include lines like the
;;;; following ones.
;;;;
;;;;   (autoload 'run-ucl "lispshell" "Run an inferior Utah Common Lisp." t)
;;;;   (autoload 'run-hpcl "lispshell" "Run an inferior HP Common Lisp." t)
;;;;   ...
;;;;
;;;; To define your own keybindings, use the hook "lispshell-load-hook".
;;;;
;;;;   ;; Make C-c C-f run command foo in lispshell-mode.
;;;;   (setq lispshell-load-hook
;;;;         (function (lambda ()
;;;;                     (define-key lispshell-mode-map "\C-c\C-f" 'foo))))
;;;;
;;;; You can change the behavior of Lisp Shells by changing the values of
;;;; certain variables; if you want to change the default values, you should do
;;;; so in a hook.
;;;;
;;;;   ;; Don't put the Lisp prompts on the mode line.
;;;;   (setq lispshell-remove-prompts nil)

(require 'comint)
(let ((current-syntax-table (syntax-table)))
  ;; Loading scheme.el has a nasty side effect -- it changes the current
  ;; buffer's syntax table to scheme-mode-syntax-table!
  (require 'scheme)
  (set-syntax-table current-syntax-table))
(provide 'lispshell)

;; Make sure that users won't accidentally load the GNU xscheme package.
(fmakunbound 'run-scheme)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Here are the global variable declarations.  (Declarations for specific
;;;; Lisp Shells -- UCL, HPCL, et al -- are at the end of the file.)
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar lispshell-mode-hook nil
  "This hook is run after entering Lispshell mode.")

(defvar lispshell-load-hook nil
  "This hook is run after the file defining Lispshell mode is loaded into GNU
Emacs.  This is a good place to define your own key bindings.")

(defvar lispshell-running-epoch (boundp 'epoch::version)
  "Set when running Epoch, an enhanced version of GNU Emacs.")

(defvar lispshell-rsh-name "/usr/ucb/rsh"
  "The name of the program used to run inferior Lisp processes on remote
machines.")

(defvar lispshell-start-two-windows t
  "*When non-nil, new Lisp Shell buffers will appear in a separate window.")

(defvar lispshell-start-new-screen t
  "*When non-nil, new Lisp Shell buffers will appear in a separate Epoch
screen.  NOTE that this variable is only meaningful when Epoch is running.")

(defvar lispshell-popup-for-output t
  "*When non-nil, Lisp Shell buffers pop up when output is received from the
associated inferior Lisp processes.")

(defvar lispshell-follow-output t
  "*When non-nil, the point in a Lisp Shell buffer follows the output from the
associated inferior Lisp process.  When nil, the point follows the output only
if it is already at or past the end of the inferior Lisp's output.

When more than one window displays a given Lisp Shell buffer, only one of those
windows follows the output.  Lispshell mode tries to choose the currently
selected window or the \"most recently used\" window on that buffer.")

(defvar lispshell-return-sends-input t
  "*When non-nil, pressing Return in a Lisp Shell buffer sends input to the
associated inferior Lisp process.  When nil, pressing Return only inserts a
newline and appropriate indentation into the buffer; it does NOT send anything
to the inferior Lisp.")

(defvar lispshell-copy-old-input t
  "*When non-nil, old input commands are copied to the end of the inferior
Lisp's output when re-entered.")

(defvar lispshell-source-modes '(lisp-mode scheme-mode)
  "Used to determine if a buffer contains Lisp source code.  If a buffer is in
one of these major modes, it's considered a Lisp source file by the commands
lispshell-load-file and lispshell-compile-file.  This variable is used by these
commands to determine defaults.")

(defvar lispshell-common-lisp-load-command
  "(load \"%s\" :verbose nil :print t)\n"
  ;; "(progn (load \"%s\" :verbose nil :print t) (fresh-line) (values))\n"
  "Format-string for building a Common Lisp expression to load a file.  See the
documentation for the variable lispshell-load-command.")

(defvar lispshell-common-lisp-compile-command
  "(compile-file \"%s\")\n"
  "Format-string for building a Common Lisp expression to compile a file.  See
the documentation for the variable lispshell-compile-command.")

(defvar lispshell-scheme-load-command
  "(load \"%s\")\n"
  "Format-string for building a Scheme expression to load a file.  See the
documentation for the variable lispshell-load-command.")

(defvar lispshell-prev-run-lisp-program nil
  "Saves the (directory . file) pair of the last program started with run-lisp
or run-remote-lisp.  This is the default for the next run-lisp or run-remote-
lisp invocation.")

(defvar lispshell-prev-run-remote-lisp-host (system-name)
  "Saves the host name of the last program started with run-remote-lisp.  This
is the default for the next run-remote-lisp invocation.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Here are the buffer-local variable declarations.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar lispshell-mru-window nil
  "The \"most recently used\" window on this Lisp Shell buffer.  This is the
window that may be scrolled in order to follow the output from the inferior
Lisp process.")

(defvar lispshell-target-buffer nil
  ;; NOTE that this variable can be buffer-local for Lisp source buffers, NOT
  ;; for Lisp Shell buffers!
  "The current Lispshell process buffer.

MULTIPLE PROCESS SUPPORT
========================

Lispshell mode is designed to allow you to run several inferior Lisp processes
at once.  Each command that creates Lisp Shell buffers uses a specific buffer
name; for example, \"run-ucl\" uses the buffer name \"*UCL*\".  If an inferior
Lisp is already running in the buffer, the command simply switches to that
existing buffer.

So how can you run more that one copy of a particular Lisp?  After you start
the first copy, rename its buffer with \\[rename-buffer].  Create the second
copy in the usual way; it will appear in a new buffer, with the usual name
\(\"*UCL*\", \"*HPCL*\", or whatever).

Commands that send text from source buffers to Lisp processes -- for example,
lispshell-eval-defun and lispshell-load-file -- have to choose a \"target\"
process to receive the text.  These are the rules --

  + If you're in a Lisp Shell buffer, the text goes to the process attached to
    the current buffer.
  + If you're in some other buffer (for example, a source file), the text goes
    to the process attached to the buffer in the variable lispshell-target-
    buffer.  NOTE that each Lisp source code buffer can have its own value for
    lispshell-target-buffer (ie, each source code buffer can be associated with
    a different Lisp Shell buffer).  See below.

When a new inferior Lisp process is created, the global lispshell-target-buffer
is usually set to be the new process' buffer.  If you only run one process at a
time, this automatically does the right thing.  If you run multiple processes
you can switch the default target back and forth with \"lispshell-set-target-
buffer\", which is bound to C-c C-t in Lispshell mode by default.

For any Lisp source code buffer, one can choose a specific Lisp Shell target to
receive text.  When a specific target is set, that buffer-specific target is
always used instead of the default target.  (Buffers that don't have a specific
target will send text to the \"default\" target, which is chosen as described
above.)  To set (or unset) a specific target for a source code buffer, use
\"lispshell-set-target-buffer\", which is bound to C-c C-t in Lisp mode by
default.")

(defvar lispshell-rerun-info nil
  "Used by lispshell-rerun to restart an inferior Lisp.  This list contains the
Lisp process name, process connection type, program name, its arguments, and
the setup function.")

(defvar lispshell-lisp-input-filter-regexp
  "\\`\\s *\\(:\\(\\w\\|\\s_\\)\\)?\\s *\\'"
  "What not to save in the inferior Lisp's input history.  Input matching this
regexp is not saved on the input history in Lispshell mode.  The default regexp
matches whitespace followed by 0 or 1 single-letter :keyword (as in :a, :c, and
so on.")

(defvar lispshell-remove-prompts t
  "*When non-nil, prompts from the inferior Lisp are screened out of the
output buffer.  The most recent prompt is placed in lispshell-mode-line-info.
When nil, prompts from the inferior Lisp are not screened out.

Also see the documentation for variables comint-prompt-regexp and lispshell-
mode-line-info.")

(defvar lispshell-mode-line-info nil
  "Contains a string that is shown on the mode line of Lisp Shell buffers.  The
string is either \"Lisp Shell\" or the most recent prompt removed from the
output of the inferior Lisp.")

(defvar lispshell-send-eof-function nil
  "The function that handles EOF (C-d) when typed at the end of the Lisp Shell
buffer.  This is a variable so specific Lisp Shells can do fancy things, such
as exiting break loops.")

(defvar lispshell-send-debugger-command-function nil
  "The function that sends debugger commands to the inferior Lisp.")

(defvar lispshell-prev-l/c-dir/file nil
  "Saves the (directory . file) pair used in the last lispshell-load-file or
lispshell-compile-file command.  This information is used to determine the
default for the next command invocation.")

(defvar lispshell-load-command "(load \"%s\")\n"
  "Format-string for building a Lisp expression to load a file.  This format
string should use %s to substitute a file name and should result in a Lisp
expression that will command the inferior Lisp to load that file.  The default
works acceptably on most Lisps.  The string \"(progn (load \\\"%s\\\" :verbose
nil :print t) (values))\\\n\" produces more informative output, but works only
in Common Lisps.")

(defvar lispshell-compile-command "(compile-file \"%s\")\n"
  "Format-string for building a Lisp expression to compile a file.  This format
string should use %s to substitute a file name and should result in a Lisp
expression that will command the inferior Lisp to load that file.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Create the Lispshell mode keymap and define some keys in Lisp and Scheme
;;;; modes.  NOTE that Lispshell mode uses the syntax and abbrev tables of Lisp
;;;; mode.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar lispshell-mode-map nil
  "Keymap used while in Lispshell mode.")

(if lispshell-mode-map
    nil
  (setq lispshell-mode-map (copy-keymap comint-mode-map))
  ;; Comint mode uses C-c C-r for comint-show-output; we use C-c C-s.
  (define-key lispshell-mode-map "\C-c\C-s" 'comint-show-output)
    
  ;; The following three come from lisp-mode-commands in lisp-mode.el.
  (define-key lispshell-mode-map "\M-\C-q" 'indent-sexp)
  (define-key lispshell-mode-map "\177" 'backward-delete-char-untabify)
  (define-key lispshell-mode-map "\t" 'lisp-indent-line)
  
  (define-key lispshell-mode-map "\C-d" 'lispshell-delchar-or-maybe-eof)
  (define-key lispshell-mode-map "\C-m" 'lispshell-send-input-or-maybe-newline)
  (define-key lispshell-mode-map "\C-c\C-m" 'lispshell-send-input)
  (define-key lispshell-mode-map "\C-c\C-t" 'lispshell-set-target-buffer)
  (define-key lispshell-mode-map "\C-c\C-d" 'lispshell-eval-defun)
  (define-key lispshell-mode-map "\C-c\C-e" 'lispshell-eval-expression)
  (define-key lispshell-mode-map "\C-c\C-r" 'lispshell-eval-region)
  (define-key lispshell-mode-map "\C-c\C-l" 'lispshell-load-file)
  (define-key lispshell-mode-map "\C-c\C-k" 'lispshell-compile-file)
  (define-key lispshell-mode-map "\C-c\C-x" 'lispshell-rerun)
  
  ;; The next two are GNU conventions.
  (define-key lispshell-mode-map "\M-\C-x" 'lispshell-eval-defun)
  (define-key lispshell-mode-map "\C-x\C-e" 'lispshell-eval-last-sexp)
  
  ;; NMODE-like commands.
  (define-key lispshell-mode-map "\C-]a" 'lispshell-send-debugger-abort)
  (define-key lispshell-mode-map "\C-]b" 'lispshell-send-debugger-backtrace)
  (define-key lispshell-mode-map "\C-]t" 'lispshell-send-debugger-backtrace)
  (define-key lispshell-mode-map "\C-]c" 'lispshell-send-debugger-continue)
  (define-key lispshell-mode-map "\C-]q" 'lispshell-send-debugger-quit)
  (define-key lispshell-mode-map "\C-]e" 'lispshell-eval-expression)
  )

(defun lispshell-install-command-keys (mode-map)
  "Install Lispshell command keys in the given mode-map."
  ;; The following keys augment source code modes so users can more easily
  ;; process Lisp code while in source code buffers.
  
  (define-key mode-map "\C-c\C-t" 'lispshell-set-target-buffer)
  (define-key mode-map "\C-c\C-p" 'lispshell-popup-target-buffer)
  (define-key mode-map "\C-c\M-p" 'lispshell-switch-to-target-buffer)
  (define-key mode-map "\C-c\C-d" 'lispshell-eval-defun)
  (define-key mode-map "\C-c\M-d" 'lispshell-eval-defun-and-go)
  (define-key mode-map "\C-c\C-e" 'lispshell-eval-expression)
  (define-key mode-map "\C-c\M-e" 'lispshell-eval-expression-and-go)
  (define-key mode-map "\C-c\C-r" 'lispshell-eval-region)
  (define-key mode-map "\C-c\M-r" 'lispshell-eval-region-and-go)
  (define-key mode-map "\C-c\C-b" 'lispshell-eval-buffer)
  (define-key mode-map "\C-c\M-b" 'lispshell-eval-buffer-and-go)
  (define-key mode-map "\C-c\C-l" 'lispshell-load-file)
  (define-key mode-map "\C-c\C-k" 'lispshell-compile-file)
  
  ;; The next two are GNU conventions.
  (define-key mode-map "\M-\C-x" 'lispshell-eval-defun)
  (define-key mode-map "\C-x\C-e" 'lispshell-eval-last-sexp)
  
  ;; NMODE-like commands.
  (define-key mode-map "\C-]a" 'lispshell-send-debugger-abort)
  (define-key mode-map "\C-]b" 'lispshell-send-debugger-backtrace)
  (define-key mode-map "\C-]t" 'lispshell-send-debugger-backtrace)
  (define-key mode-map "\C-]c" 'lispshell-send-debugger-continue)
  (define-key mode-map "\C-]q" 'lispshell-send-debugger-quit)
  (define-key mode-map "\C-]e" 'lispshell-eval-expression)
  (define-key mode-map "\C-]p" 'lispshell-popup-target-buffer)
  )

(lispshell-install-command-keys lisp-mode-map)
(lispshell-install-command-keys scheme-mode-map)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Here are the important functions for creating Lisp Shell buffers and
;;;; processes: lispshell-mode, the filter functions for input to and output
;;;; from an inferior Lisp process, the function for locating old input forms,
;;;; lispshell-run, lispshell-rerun, and lispshell-find-executable.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(put 'lispshell-mode 'mode-class 'special)

(defun lispshell-mode ()
  "Major mode for interacting with an inferior Lisp process.  For information
about running multiple processes in multiple buffers, see the documentation for
the variable lispshell-target-buffer.
\\{lispshell-mode-map}
COMMANDS:

Return can be configured to send input to the inferior Lisp or to simply insert
  a newline and appropriate indentation; see the documentation for variable
  lispshell-return-sends-input.  When lispshell-return-sends-input is non-nil,

  + Return after the end of the process' output sends the text from the end of
    the process' output to point.
  + Return before the end of the process' output copies the current form
    (excluding any prompts) to the end of the process' output and sends it.
    See the documentation for function lispshell-get-old-input.

Delete converts tabs to spaces as it moves back.
Tab indents as appropriate for Lisp; with argument, shifts rest of
  expression rigidly with the current line.
M-C-q does Tab on each line starting with the following expression.

Paragraphs are separated only by blank lines.  Semicolons start comments.
\\<lispshell-mode-map>
An input history is maintained and can be accessed with the commands
comint-previous-input (\\[comint-previous-input]) and comint-next-input
\(\\[comint-next-input]).  The size of the history is determined by the
variable input-ring-size.

SENDING CODE TO AN INFERIOR LISP:

You can send text from a Lisp source buffer to the process associated with a
Lisp Shell buffer.  See the documentation for lisp-mode or scheme-mode for key
bindings of the following commands.

  lispshell-popup-target-buffer      Display Lisp Shell buffer in another
                                     window
  lispshell-switch-to-target-buffer  Display Lisp Shell buffer in another
                                     window and go there

  lispshell-eval-defun               Send current defun to an inferior Lisp
  lispshell-eval-defun-and-go        Send current defun to an inferior Lisp and
                                     go to its buffer
  lispshell-eval-expression          Send current sexp to an inferior Lisp
  lispshell-eval-expression-and-go   Send current sexp to an inferior Lisp and
                                     go to its buffer
  lispshell-eval-region              Send current region to an inferior Lisp
  lispshell-eval-region-and-go       Send current region to an inferior Lisp
                                     and go to its buffer
  lispshell-eval-buffer              Send current buffer to an inferior Lisp
  lispshell-eval-buffer-and-go       Send current buffer to an inferior Lisp
                                     and go to its buffer
  lispshell-eval-last-sexp           Send previous sexp to an inferior Lisp

  lispshell-load-file                Tell an inferior Lisp to load a file
  lispshell-compile-file             Tell an inferior Lisp to compile a file

Use lispshell-set-target-buffer to choose between several concurrent inferior
Lisps; see the documentation for the variable lispshell-target-buffer.

CUSTOMIZATION:

Entry into this mode runs the hooks in comint-mode-hook and lispshell-mode-
hook, in that order."
  (interactive)
  (or (eq major-mode 'comint-mode)
      (comint-mode))
  (setq major-mode 'lispshell-mode)
  (setq mode-name "Lisp Shell")
  
  ;; Set up a lot of standard lisp-mode stuff.  This buffer will now use the
  ;; lisp-mode-syntax-table and lisp-mode-abbrev-table.  See lisp-mode.el in
  ;; the GNU Emacs distribution.
  (lisp-mode-variables t)
  
  (make-local-variable 'lispshell-mru-window)
  (make-local-variable 'lispshell-rerun-info)
  (make-local-variable 'lispshell-lisp-input-filter-regexp)
  (make-local-variable 'lispshell-remove-prompts)
  (make-local-variable 'lispshell-mode-line-info)
  (make-local-variable 'lispshell-send-eof-function)
  (make-local-variable 'lispshell-send-debugger-command-function)
  (make-local-variable 'lispshell-prev-l/c-dir/file)
  (make-local-variable 'lispshell-load-command)
  (make-local-variable 'lispshell-compile-command)
  
  ;; The other comint communication stuff is okay by default.
  (setq comint-input-filter (function lispshell-lisp-input-filter))
  (setq comint-get-old-input (function lispshell-get-old-input))
  
  ;; Dink with the mode line -- only use as much space as necessary to show the
  ;; buffer name, and replace the variables mode-name and minor-mode-alist
  ;; with lispshell-mode-line-info.
  (setq mode-line-format
	'("" mode-line-modified mode-line-buffer-identification "   "
	  global-mode-string
	  "   %[(" lispshell-mode-line-info mode-line-process ")%]----"
	  (-3 . "%p") "-%-"))
  (setq mode-line-buffer-identification (if lispshell-running-epoch
					    '("Epoch: %b")
					  '("Emacs: %b")))
  (setq lispshell-mode-line-info mode-name)
  
  (use-local-map lispshell-mode-map)
  (run-hooks 'lispshell-mode-hook))

(defun lispshell-lisp-input-filter (string)
  "The filter for entering user commands into a buffer's input history.  Don't
save anything that matches lispshell-lisp-input-filter-regexp."
  (not (string-match lispshell-lisp-input-filter-regexp string)))

(defun lispshell-lisp-output-filter (process string)
  "The filter for placing output from an inferior Lisp process into its Lisp
Shell buffer.  If lispshell-remove-prompts in non-nil, input prompts are
screened out and the most recent prompt is placed in lispshell-mode-line-info."
  (let ((inhibit-quit t) ;; For GNU Emacs versions <= 18.55.
	(current-buffer (current-buffer))
	(buffer (process-buffer process)))
    (set-buffer buffer)
    (let ((old-point (point))
	  (old-end (marker-position (process-mark process)))
	  (buffer-window (lispshell-get-mru-window buffer)))
      (goto-char old-end)
      (insert string)
      (set-marker (process-mark process) (point))
      (set-buffer-modified-p t)
      
      (cond (lispshell-remove-prompts
	     ;; UCL sometimes spits out more than one prompt -- get 'em all.
	     (let ((old-match-data (match-data))
		   (found-prompt nil))
	       (while (re-search-backward comint-prompt-regexp old-end t)
		 (or found-prompt
		     (setq found-prompt (buffer-substring (match-beginning 0)
							  (match-end 0))))
		 (replace-match "" t t))
	       (store-match-data old-match-data)
	       (if found-prompt
		   (setq lispshell-mode-line-info found-prompt)))
	     (goto-char (marker-position (process-mark process)))))
      
      (if (or lispshell-follow-output (>= old-point old-end))
	  (progn
	    ;; If we were after the old end-of-output, replace point; that is,
	    ;; put it back where it started.
	    (if (> old-point old-end)
		(forward-char (- old-point old-end)))
	    ;; If the Lisp Shell buffer is visible, make sure that the window
	    ;; follows the output.
	    (if buffer-window
		(set-window-point buffer-window (point))))
	;; Otherwise, don't follow the output -- move point back.
	(goto-char old-point))
      
      (cond ((not lispshell-popup-for-output) nil)
	    ((null buffer-window)
	     (let ((pop-up-windows t))
	       (display-buffer buffer nil)))
	    (lispshell-running-epoch
	     (mapraised-screen (screen-of-window buffer-window)))))
    
    (set-buffer current-buffer)))

(defun lispshell-get-mru-window (buffer)
  ;; Locate and return the "most recently used" window on the given Lisp Shell
  ;; buffer.  If no window shows the buffer, return nil.  NOTE that this
  ;; function assumes that the current buffer is the same as argument buffer
  ;; (so we can get the buffer-local value of lispshell-mru-window).
  (let* ((selected-window (selected-window))
	 (selected-buffer (window-buffer selected-window)))
    (cond ((eq buffer selected-buffer)
	   (setq lispshell-mru-window selected-window)
	   selected-window)
	  ((and lispshell-mru-window (window-point lispshell-mru-window)
		;; window-point is nil if the window has been deleted.
		(eq buffer (window-buffer lispshell-mru-window)))
	   lispshell-mru-window)
	  (t (let ((window (if lispshell-running-epoch
			       (epoch::get-buffer-window buffer)
			     (get-buffer-window buffer))))
	       (setq lispshell-mru-window window)
	       window)))))

(defun lispshell-get-old-input ()
  "Find the \"old input\" near point.  This function tries to locate the
complete form near point; here's a description of how it works.

  + First move to the beginning of the current line, and then move forward
    past blank lines and comments.
  + If the current line starts with a prompt from the inferior Lisp, collect
    the text from all the adjacent lines that start with that prompt.  That
    text is the \"old input.\"
  + Otherwise, if the current line is the beginning of a defun (ie, begins with
    an opening parenthesis), then that defun is the \"old input.\"
  + Otherwise, if the current line starts with whitespace, determine if the
    current line is part of a defun.  If so, the entire defun is the \"old
    input.\"
  + Otherwise, the current line is the \"old input.\"

This function moves point to the end of the old input (usually, the beginning
of the line after the old input)."
  (beginning-of-line)
  ;; Skip over blank lines and comments.
  (while (and (looking-at "^\\s-*\\(;\\|$\\)")
	      (search-forward "\n" (point-max) t))
    )
  (cond ((and (eq major-mode 'lispshell-mode)
	      (looking-at comint-prompt-regexp))
	 ;; Get all the lines around here that begin with this prompt.
	 (let ((this-prompt
		(regexp-quote (buffer-substring (match-beginning 0)
						(match-end 0))))
	       (looking t)
	       (piece-list nil)
	       (end-point nil))
	   ;; First, move to the end of this set of lines.
	   (while looking
	     (if (search-forward "\n" (point-max) t)
		 (setq looking (looking-at this-prompt))
	       (progn
		 ;; Can't move past this last line, so grab its text now.
		 (setq looking nil)
		 (setq piece-list
		       (cons "\n" (cons (buffer-substring (match-end 0)
							  (progn (end-of-line)
								 (point)))
					piece-list))))))
	   ;; Remember this final point.
	   (setq end-point (point))
	   ;; Now collect the text from previous lines.
	   (while (and (= (forward-line -1) 0)
		       (looking-at this-prompt))
	     (setq piece-list
		   (cons "\n" (cons (buffer-substring (match-end 0)
						      (progn (end-of-line)
							     (point)))
				    piece-list))))
	   (goto-char end-point)
	   (apply (symbol-function 'concat) (cdr piece-list))))
	
	((looking-at "\\s(")
	 ;; We're looking at the start of a defun, so grab it.  Leave off
	 ;; any trailing newline.
	 (buffer-substring (point) (progn (end-of-defun) (if (bolp)
							     (- (point) 1)
							   (point)))))
	
	((looking-at "\\s-")
	 ;; We're on a non-blank line that begins with whitespace.  We might
	 ;; be within a defun -- find out.
	 (let ((old-point (point)))
	   (beginning-of-defun)
	   (if (looking-at "\\s(")
	       ;; We found the beginning of a defun; does the defun enclose
	       ;; the old-point?
	       (let ((beginning (point)))
		 (end-of-defun)
		 (if (and (<= beginning old-point)
			  (<= old-point (point)))
		     ;; Yes, we have a defun.
		     (buffer-substring beginning (if (bolp)
						     (- (point) 1)
						   (point)))
		   ;; No, we don't have a defun.  Just send the line.
		   (prog2
		     (goto-char old-point)
		     (buffer-substring (point) (progn (end-of-line) (point)))
		     (forward-line))))
	     ;; We couldn't find the beginning of a defun, so just send the
	     ;; line.
	     (prog2
	       (goto-char old-point)
	       (buffer-substring (point) (progn (end-of-line) (point)))
	       (forward-line)))))
	
	(t
	 (prog1
	   (buffer-substring (point) (progn (end-of-line) (point)))
	   (forward-line)))))

(defun lispshell-get-screen (buffer screen-title)
  "Find and return an Epoch screen to display the given buffer.  First look for
screens that are already displaying the buffer.  If there are none, look for a
screen with the given title.  If no screen with that title exists, create and
return a new screen with that title."
  (let ((buffer-screens (screens-of-buffer buffer)))
    (if buffer-screens
	(car buffer-screens)
      (let ((screens (screen-list t))
	    (screen-with-title nil))
	(while (and screens
		    (not screen-with-title))
	  (if (string-equal screen-title (title nil (car screens)))
	      (setq screen-with-title (car screens))
	    (setq screens (cdr screens))))
	(if screen-with-title
	    screen-with-title
	  (create-screen buffer (list (cons 'title screen-title)
				      '(update  . t))))))))

(defun lispshell-run (program arglist host-name buffer-name setup-function)
  "Run an inferior Lisp process with input and output through a buffer.  If a
process is already running in the named buffer, simply switch to that buffer.

The inferior Lisp process is usually run on the local machine.  However, if the
argument HOST-NAME is non-nil, then the inferior Lisp is run (via a remote
shell) on the machine named in that argument.

The newly created (or already existing) buffer becomes the default \"target\"
buffer; see the documentation for variable lispshell-target-buffer."
  ;; I rewrote this to avoid make-comint, because make-comint resets comint
  ;; mode and blasts all the local variables.
  (let* ((buffer-name-with-stars
	  (if host-name
	      (concat "*" buffer-name "(" host-name ")*")
	    (concat "*" buffer-name "*")))
	 (buffer (get-buffer-create buffer-name-with-stars))
	 (process (get-buffer-process buffer)))
    (cond ((not (and process (memq (process-status process) '(run stop))))
	   (if host-name
	       ;; Plan to run the inferior Lisp remotely, via a remote shell.
	       (progn (setq arglist (cons host-name (cons program arglist)))
		      (setq program lispshell-rsh-name)))
	   (set-buffer (comint-exec buffer buffer-name program nil arglist))
	   (or (eq major-mode 'lispshell-mode)
	       (lispshell-mode))
	   (set-process-filter (get-buffer-process (current-buffer))
			       (function lispshell-lisp-output-filter))
	   (setq lispshell-rerun-info
		 (list buffer-name process-connection-type program arglist
		       setup-function))
	   (funcall setup-function)))
    (setq-default lispshell-target-buffer buffer)
    
    (if (and lispshell-running-epoch lispshell-start-new-screen)
	(let ((screen (lispshell-get-screen buffer buffer-name-with-stars)))
	  (mapraised-screen screen)
	  (if (not (eq (current-screen) screen))
	      (cursor-to-screen screen))
	  (select-screen screen)))
    
    (if lispshell-start-two-windows
	(let ((pop-up-windows t))
	  (pop-to-buffer buffer nil))
      (switch-to-buffer buffer))))

(defun lispshell-rerun ()
  "Run an inferior Lisp process with input and output through the current
buffer.  The new process uses the same Lisp program, arguments, and host
machine as the process that previously ran in the current buffer; in short, the
process \"reruns\" the Lisp that was associated with the current buffer.

If a process is already associated with the current buffer, no new process is
created.  The old one is continued if it had been stopped.

The current buffer does NOT become the default \"target\" buffer; see the
documentation for variable lispshell-target-buffer."
  (interactive)
  (let ((process (get-buffer-process (current-buffer))))
    ;; Make sure that we're in a Lisp Shell buffer and have no viable process.
    (cond ((not (eq major-mode 'lispshell-mode))
	   (error "Current buffer is not a Lisp Shell buffer."))
	  (process
	   (let ((status (process-status process))
		 (name (process-name process)))
	     (cond ((eq status 'run)
		    (error "Process %s is already running." name))
		   ((eq status 'stop)
		    (comint-continue-subjob)
		    (error "Process %s was stopped and is now running." name))
		   )))
	  ((not lispshell-rerun-info) 
	   (error "No Lisp has been run in the current buffer."))))
  
  ;; Now restart the previously-run Lisp program.  We don't make the current
  ;; buffer the default target because the old argument list may contain
  ;; filenames (which make Utah Lisp compilers to go into "batch" mode).  Maybe
  ;; this is confusing and should be fixed.
  (let ((process-name (nth 0 lispshell-rerun-info))
	(process-connection-type (nth 1 lispshell-rerun-info))
	;; process-connection-type is used by comint-exec.
	(program (nth 2 lispshell-rerun-info))
	(arglist (nth 3 lispshell-rerun-info))
	(setup-function (nth 4 lispshell-rerun-info)))
    (comint-exec (current-buffer) process-name program nil arglist)
    (set-process-filter (get-buffer-process (current-buffer))
			(function lispshell-lisp-output-filter))
    (funcall setup-function)))

(defun lispshell-find-executable (specific-name path-variable default-name)
  ;; Determine the name of an inferior Lisp executable.  If a specific name is
  ;; specified (is non-nil), return it.  Otherwise use the specified
  ;; environment variable and default name to locate the executable.
  (or specific-name
      (let ((path (getenv path-variable)))
	(or (if path
		(let ((specific-name (concat path default-name)))
		  (if (file-exists-p specific-name)
		      specific-name)))
	    ;; Either the path variable isn't set or the executable isn't
	    ;; where we expected it to be.  Return the non-directory part of
	    ;; the default name, and we'll use the normal search path.
	    (file-name-nondirectory default-name)))))
      
(defun lispshell-file-args (prompt-flag prompt)
  ;; When a source file is entered, check that its buffer has been saved and
  ;; remember the file to use as the future default.
  (if prompt-flag
      (let ((source-file-name
	     (comint-get-source prompt (or lispshell-prev-l/c-dir/file
					   (lispshell-recently-visited-source))
				lispshell-source-modes nil)))
	(comint-check-source (car source-file-name))
	(setq lispshell-prev-l/c-dir/file
	      (cons (file-name-directory (car source-file-name))
		    (file-name-nondirectory (car source-file-name))))
	source-file-name)
    nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Here are functions for dealing with the target buffer and process.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun lispshell-target-process ()
  "Returns the current inferior Lisp process -- the \"target\" for commands
that send text from source buffers to inferior Lisps.  See the documentation
for the variable lispshell-target-buffer."
  (let* ((buffer (if (eq major-mode 'lispshell-mode)
		     (current-buffer)
		   (and lispshell-target-buffer
			(get-buffer lispshell-target-buffer))))
	 (process (and buffer (get-buffer-process buffer))))
    (or process
      (error
       (if (and buffer (buffer-name buffer))
	   ;; buffer-name is nil if the buffer has been killed.
	   (format
	    "Buffer %s has no process.  See variable lispshell-target-buffer."
	    (buffer-name buffer))
	 "No current Lisp Shell buffer.  See variable lispshell-target-buffer."
	 )))))

(defun lispshell-set-target-buffer ()
  "Select a buffer to be the \"target buffer\" for commands that send text from
source buffers to inferior Lisp processes.

If this command is invoked from a Lisp Shell buffer, then the current buffer
becomes the default target buffer.  If this command is invoked from a Lisp
source code buffer, then the user can set (or unset) a specific target for the
current buffer.  When no specific target for a buffer is set, commands that
send text will send to the default target.

See the documentation for variable lispshell-target-buffer."
  (interactive)
  (cond ((eq major-mode 'lispshell-mode)
	 (setq lispshell-target-buffer (current-buffer))
	 (message "Buffer %s is now the default target Lisp Shell."
		  (buffer-name)))
	
	((memq major-mode lispshell-source-modes)
	 (let ((lispshell-buffer-alist
		(lispshell-set-target-buffer-aux (buffer-list)))
	       target-buffer-name)
	   (if lispshell-buffer-alist
	       (let ((target-buffer-name
		      (completing-read
		       (format
			"Set specific target buffer for %s: (default none) "
			(buffer-name))
		       lispshell-buffer-alist nil t nil)))
		 (if (> (length target-buffer-name) 0)
		     ;; Make a buffer-local copy of lispshell-target-buffer.
		     (progn
		       (make-local-variable 'lispshell-target-buffer)
		       (setq lispshell-target-buffer
			     (cdr (assoc target-buffer-name
					 lispshell-buffer-alist))))
		   ;; Otherwise, destroy any local copy of the variable.
		   (kill-local-variable 'lispshell-target-buffer)))
	     (error "There are no Lisp Shell buffers."))))
	
	(t (error
	    "Current buffer is not a Lisp Shell or Lisp source code buffer."
	    ))))

(defun lispshell-set-target-buffer-aux (buffer-list)
  ;; Return an association list of names and Lisp Shell buffers.
  (if buffer-list
      (let ((the-buffer (car buffer-list))
	    (rest (lispshell-set-target-buffer-aux (cdr buffer-list))))
	(if (eq 'lispshell-mode (save-excursion (set-buffer the-buffer)
						major-mode))
	    (cons (cons (buffer-name the-buffer) the-buffer) rest)
	  rest))
    nil))

(defun lispshell-popup-target-buffer ()
  "Display the current Lisp Shell buffer in another window.  In Epoch, if the
buffer is displayed on some screen, then that screen is mapped and raised."
  (interactive)
  (let ((buffer (and lispshell-target-buffer
		     (get-buffer lispshell-target-buffer)))
	buffer-window)
    (if (and buffer (buffer-name buffer))
	;; buffer-name is nil if the buffer has been killed.
	(if (and lispshell-running-epoch
		 (setq buffer-window (epoch::get-buffer-window buffer)))
	    (mapraised-screen (screen-of-window buffer-window))
	  (let ((pop-up-windows t))
	    (display-buffer buffer nil)))
      (error
       "No current Lisp Shell buffer.  See variable lispshell-target-buffer."))
    ))

(defun lispshell-switch-to-target-buffer (end-of-output-p)
  "Switch to the current Lisp Shell buffer.  With argument, move point to the
end of the inferior Lisp's output (or if no process is running, the end of the
buffer).  In Epoch, if the buffer is displayed on some screen, then that screen
is mapped and raised."
  (interactive "P")
  (let ((buffer (and lispshell-target-buffer
		     (get-buffer lispshell-target-buffer)))
	buffer-window)
    (if (and buffer (buffer-name buffer))
	;; buffer-name is nil if the buffer has been killed.
	(progn
	  (setq buffer-window (save-excursion
				(set-buffer buffer) ;; Gotta go there first...
				(lispshell-get-mru-window buffer)))
	  (cond ((and lispshell-running-epoch buffer-window)
		 (let ((window-screen (screen-of-window buffer-window)))
		   (mapraised-screen window-screen)
		   (if (not (eq (current-screen) window-screen))
		       (cursor-to-screen window-screen))
		   (select-window buffer-window)))
		(buffer-window
		 (select-window buffer-window))
		(t (pop-to-buffer buffer nil))))
      (error
       "No current Lisp Shell buffer.  See variable lispshell-target-buffer."))
    (cond (end-of-output-p
	   (push-mark)
	   (let ((process (get-buffer-process buffer)))
	     (goto-char (if process
			    (marker-position (process-mark process))
			  (point-max))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Here are functions for sending text to inferior Lisp processes.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun lispshell-send-eof ()
  "Call function in variable lispshell-send-eof-function to send an EOF or
other useful EOF-like string to the inferior Lisp process."
  (interactive)
  (funcall lispshell-send-eof-function (lispshell-target-process)))

(defun lispshell-delchar-or-maybe-eof (arg)
  "If not at end of buffer, delete ARG characters forward.  If at end of
buffer, call function in variable lispshell-send-eof-function to send an EOF or
other useful EOF-like string to the inferior Lisp process."
  (interactive "p")
  (if (eobp)
      (funcall lispshell-send-eof-function
	       (get-buffer-process (current-buffer)))
    (delete-char arg)))

(defun lispshell-send-input ()
  "Send input to the inferior Lisp process.

  + When point is after (or at) the process output mark, all the text between
    the process output mark and point is sent as input.
  + When point is before the process output mark, the function in variable
    comint-get-old-input is called to locate old input.  The old input is
    copied to the process output mark if lispshell-copy-old-input is non-nil.
    Finally, the old input is sent to the inferior Lisp process.

In either case, the function in variable comint-sentinel is called before the
input is sent.  The input is entered into the input history ring if the
function in variable comint-input-filter returns a non-nil value when called
with the input.

This function is a replacement for function comint-send-input."
  (interactive)
  ;; NOTE that the input string does not contain its terminating newline.
  ;; Also, this function should only be invoked while point is in a Lisp Shell
  ;; buffer.
  (let ((process (get-buffer-process (current-buffer))))
    (if (not process)
	(error "Current buffer has no process.")
      (let* ((process-mark (process-mark process))
	     (process-mark-value (marker-position process-mark))
	     (input (if (>= (point) process-mark-value)
			(progn (if comint-eol-on-send (end-of-line))
			       (prog1 (buffer-substring process-mark (point))
				      (insert ?\n)))
		      (let ((old-input (funcall comint-get-old-input)))
			(if lispshell-copy-old-input
			    (progn (goto-char process-mark)
				   (insert old-input)
				   (insert ?\n)))
			old-input))))
	
	(if (funcall comint-input-filter input)
	    (ring-insert input-ring input))
	(funcall comint-input-sentinel input)
	(funcall comint-input-sender process input)
	(if (or (>= (point) process-mark-value) lispshell-copy-old-input)
	    ;; Point is at the place we except to see the process output.
	    (progn (set-marker (process-mark process) (point))
		   (set-marker comint-last-input-end (point)))
	  ;; Otherwise, we expect to see process output immediately after the
	  ;; previous output.  NOTE that comint-last-input-end is used by
	  ;; comint-kill-output, and that's why we update it here.
	  (set-marker comint-last-input-end process-mark-value))))))

(defun lispshell-send-input-or-maybe-newline ()
  "If lispshell-return-sends-input is non-nil, then send input to the inferior
Lisp process.  Otherwise, simply insert a newline and appropriate indentation.

See also the documentation for function lispshell-send-input."
  (interactive)
  (if lispshell-return-sends-input
      (lispshell-send-input)
    (newline-and-indent)))

(defun lispshell-send-eval-string (input)
  "Send the given input string to the inferior Lisp process.  This function is
the workhorse for the lispshell-eval-* functions, which send various strings to
the inferior Lisp process.

When point is within a Lisp Shell buffer, the input is entered into the input
history when the function in variable comint-input-filter returns a non-nil
value when called with the input.  Also, if point is after the process output
mark, then the process output mark is advanced to point (so that output will
presumably appear after the form just sent)."
  (let* ((process (lispshell-target-process))
	 (process-mark (process-mark process))
	 (process-mark-value (marker-position process-mark)))
    (if (eq major-mode 'lispshell-mode)
	;; When we're in a Lisp Shell buffer, we want to maintain the history
	;; and make sure that output appears where we expect it.
	(progn (if (funcall comint-input-filter input)
		   (ring-insert input-ring input))
	       (if (>= (point) process-mark-value)
		   (progn (if (not (bolp)) (insert ?\n))
			  (set-marker (process-mark process) (point)))))
      ;; Otherwise, we need to go to the target buffer to find the input
      ;; sentinel and sender.  NOTE that we don't have to check that the target
      ;; buffer and process are still alive; lispshell-target-process already
      ;; did that.
      (set-buffer (get-buffer lispshell-target-buffer)))
    (funcall comint-input-sentinel input)
    (funcall comint-input-sender process input)
    (set-marker comint-last-input-end process-mark-value)))

(defun lispshell-send-debugger-command (command)
  ;; Find and call the function that knows what text to send to the inferior
  ;; Lisp.
  (let ((process (lispshell-target-process)))
    (funcall (save-excursion
	       (set-buffer (process-buffer process))
	       lispshell-send-debugger-command-function) process command)))

(defun lispshell-send-debugger-abort ()
  "Send an \"abort\" command to the inferior Lisp's debugger."
  (interactive)
  (lispshell-send-debugger-command 'abort))

(defun lispshell-send-debugger-backtrace ()
  "Send a \"backtrace\" command to the inferior Lisp's debugger."
  (interactive)
  (lispshell-send-debugger-command 'backtrace))

(defun lispshell-send-debugger-continue ()
  "Send an \"continue\" command to the inferior Lisp's debugger."
  (interactive)
  (lispshell-send-debugger-command 'continue))

(defun lispshell-send-debugger-quit ()
  "Send a \"quit\" command to the inferior Lisp's debugger."
  (interactive)
  (lispshell-send-debugger-command 'quit))

(defun lispshell-eval-region (start end)
  "Send the current region to the inferior Lisp process."
  (interactive "r")
  (save-excursion
    (goto-char end)
    (lispshell-send-eval-string (buffer-substring start end))))

(defun lispshell-eval-region-and-go (start end)
  "Send the current region to the inferior Lisp process, and switch to the
process' buffer."
  (interactive "r")
  (lispshell-eval-region start end)
  (lispshell-switch-to-target-buffer t))

(defun lispshell-eval-defun ()
  "Send the current defun to the inferior Lisp process.  The \"current defun\"
is determined as follows:

If point is within a defun, then that defun is the current defun.
Otherwise, if a defun ends on the current line, then it is the current defun.
Otherwise, if there is a defun after point, then the nearest defun after point
  is the current defun.
Otherwise, the nearest defun before point is the current defun.

NOTE that this command moves point to the end of the current defun."
  (interactive)
  ;; It looks like we bounce around a lot in this function, but it's what we
  ;; really need to do.
  (let ((old-point (point)))
    (beginning-of-line)
    (end-of-defun)
    (if (beginning-of-defun)
	(let ((beginning (point)))
	  (end-of-defun)
	  (lispshell-send-eval-string (buffer-substring beginning
							(if (bolp)
							    (- (point) 1)
							  (point)))))
      (goto-char old-point)
      (error "Can't find a defun."))))

(defun lispshell-eval-defun-and-go ()
  "Send the current defun to the inferior Lisp process, and switch to the
process' buffer.  See the documentation for function lispshell-eval-defun to
understand how the \"current defun\" is found."
  (interactive)
  (lispshell-eval-defun)
  (lispshell-switch-to-target-buffer t))

(defun lispshell-eval-expression ()
  "Send the current sexp (symbolic expression) to the inferior Lisp process.
See the documentation for function lispshell-get-old-input to understand how
the \"current expression\" is found."
  (interactive)
  (lispshell-send-eval-string (lispshell-get-old-input)))

(defun lispshell-eval-expression-and-go ()
  "Send the current sexp (symbolic expression) to the inferior Lisp process,
and switch to the process' buffer.  See the documentation for function
lispshell-get-old-input to understand how the \"current expression\" is found."
  (interactive)
  (lispshell-send-eval-string (lispshell-get-old-input))
  (lispshell-switch-to-target-buffer t))

(defun lispshell-eval-last-sexp ()
  "Send the previous sexp to the inferior Lisp process.  This command is not
nearly as sophisticated as lispshell-eval-expression; try using it instead."
  (interactive)
  (lispshell-eval-region (save-excursion (backward-sexp) (point)) (point)))

(defun lispshell-eval-buffer ()
  "Send the contents of the current buffer to the inferior Lisp process."
  (interactive)
  (lispshell-eval-region (point-min) (point-max)))

(defun lispshell-eval-buffer-and-go ()
  "Send the contents of the current buffer to the inferior Lisp process, and
switch to the process' buffer."
  (interactive)
  (lispshell-eval-buffer)
  (lispshell-switch-to-target-buffer t))

(put 'lispshell-eval-buffer 'disabled t)
(put 'lispshell-eval-buffer-and-go 'disabled t)

(defun lispshell-load-file (file-name)
  "Load a Lisp file into the inferior Lisp process, and switch to the process'
buffer."
  (interactive
   ;; Load doesn't require an exact match (so last arg is nil).
   (comint-get-source "Load Lisp file: "
		      (or lispshell-prev-l/c-dir/file
			  (lispshell-recently-visited-source))
		      lispshell-source-modes nil))
  (comint-check-source file-name)
  
  ;; NOTE that by switching here, we'll be refering to the buffer-local copies
  ;; of lispshell-prev-l/c-dir/file and lispshell-load-command, which is what
  ;; we want to do.
  (or (eq major-mode 'lispshell-mode)
      (lispshell-switch-to-target-buffer t))
  (setq lispshell-prev-l/c-dir/file (cons (file-name-directory file-name)
					  (file-name-nondirectory file-name)))
  (comint-send-string (lispshell-target-process)
		      (format lispshell-load-command file-name)))

(defun lispshell-compile-file (file-name)
  "Compile a Lisp file in the inferior Lisp process, and switch to the process'
buffer."
  (interactive
   ;; Again, don't require an exact match.
   (comint-get-source "Compile Lisp file: "
		      (or lispshell-prev-l/c-dir/file
			  (lispshell-recently-visited-source))
		      lispshell-source-modes nil))
  (comint-check-source file-name)
  
  ;; NOTE that by switching here, we'll be refering to the buffer-local copies
  ;; of lispshell-prev-l/c-dir/file and lispshell-compile-command, which is
  ;; what we want to do.
  (or (eq major-mode 'lispshell-mode)
      (lispshell-switch-to-target-buffer t))
  (setq lispshell-prev-l/c-dir/file (cons (file-name-directory file-name)
					  (file-name-nondirectory file-name)))
  (comint-send-string (lispshell-target-process)
		      (format lispshell-compile-command file-name)))

(defun lispshell-recently-visited-source ()
  ;; Find the most recently displayed Lisp source buffer and return the name of
  ;; the file it is visiting (as a directory-and-file pair).
  (let ((buffer-list (buffer-list))
	(directory-and-file nil))
    (while (and buffer-list (not directory-and-file))
      (if (memq (save-excursion (set-buffer (car buffer-list)) major-mode)
		lispshell-source-modes)
	  ;; We have a source buffer; if it's visiting a file, get its name.
	  (let ((file-name (buffer-file-name (car buffer-list))))
	    (if file-name
		(setq directory-and-file
		      (cons (file-name-directory file-name)
			    (file-name-nondirectory file-name))))))
      (setq buffer-list (cdr buffer-list)))
    directory-and-file))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Here are the functions for running particular Lisp interpreters.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;
;;;; The functions for Utah Common Lisp (UCL).
;;;;

(defvar ucl-prompt-regexp "\\b[0-9]+ \\((debug [0-9]+) \\)?\\[[^]]*\\]-> "
  "The regexp for matching prompts from Utah Common Lisp.")

(defvar ucl-input-filter-regexp "\\`\\s *\\(:\\w\\w?\\s *[0-9]*\\)?\\s *\\'"
  "The regexp for matching most Utah Common Lisp debugger commands.")

(defvar ucl-debugger-command-alist
  '((abort . ":a\n") (backtrace . ":t\n") (continue . ":c\n") (quit . ":q\n"))
  "Association list of symbols and Utah Common Lisp debugger command strings.")

(defvar ucl-extra-heap nil
  "*The amount of extra heap space to allocate (expressed as a percentage of
the normal heap space) for Utah Common Lisp and other Utah Lisps.  This
percentage must be between 25 and 100 inclusive.  If this variable is nil, the
inferior Lisp will allocate a normal-sized heap.")

(defvar ucl-cputime-limit nil
  "*The CPU time limit (in seconds) for Utah Common Lisp and other Utah Lisps.
If this variable is nil, the inferior Lisp process will set its own limit.")

(defvar run-ucl-hook nil
  "This hook is run after a new Utah Common Lisp process is created.")

(defun run-ucl (&optional program-name host-name)
  "Run a Utah Common Lisp process with input and output via pipes through
buffer *UCL*.  If a process is already running in buffer *UCL*, simply switch
to that buffer.

Buffer *UCL* becomes the default \"target\" buffer; see the documentation for
variable lispshell-target-buffer.

When called interactively, the environment variable UCL is examined to find the
program to run.  If UCL isn't set or $UCL/bin/ucl doesn't exist, use the normal
search path.

When called interactively, the process is created on the local machine.
Programs can use \"run-ucl\" to create processes on remote machines; in this
case, the buffer name includes the remote machine's name.

When a new process is created, the hook run-ucl-hook is executed."
  (interactive (list nil nil))
  (let ((process-connection-type nil))
    (lispshell-run (lispshell-find-executable program-name "UCL" "/bin/ucl")
		   (cons "-i" (nconc (ucl-heapsize-args) (ucl-cpulimit-args)))
		   host-name "UCL"
		   (function ucl-setup))))

(defun ucl-heapsize-args ()
  (if (and (numberp ucl-extra-heap)
	   (>= ucl-extra-heap 25) (<= ucl-extra-heap 100))
      (list "-g" (int-to-string ucl-extra-heap))
    nil))

(defun ucl-cpulimit-args ()
  (if (and (numberp ucl-cputime-limit) (> ucl-cputime-limit 0))
      (list "-u" (int-to-string ucl-cputime-limit))
    nil))

(defun ucl-setup ()
  (setq comint-prompt-regexp ucl-prompt-regexp)
  (setq lispshell-lisp-input-filter-regexp ucl-input-filter-regexp)
  (setq lispshell-send-eof-function (function ucl-send-eof))
  (setq lispshell-send-debugger-command-function
	(function ucl-send-debugger-command))
  (setq lispshell-load-command lispshell-common-lisp-load-command)
  (setq lispshell-compile-command lispshell-common-lisp-compile-command)
  (run-hooks 'run-ucl-hook))

(defun ucl-send-eof (process)
  ;; Because we're talking through pipes, we have to send this form and not
  ;; just a simple C-d.
  (comint-send-string process "(ucl::handle-eof)\n"))

(defun ucl-send-debugger-command (process command)
  (let ((command-string (cdr (assq command ucl-debugger-command-alist))))
    (if command-string
	(comint-send-string process command-string)
      (error "No command string for \"%s\" has been defined." command))))

;;;;
;;;; The functions for Utah Common Lisp with the compiler.  The student version
;;;; of uclcomp is not intended to be interactive; instead, the user specifies
;;;; the file to be compiled on the command line.
;;;;

(defvar uclcomp-prompt-for-file t
  "*When non-nil, the command run-uclcomp prompts for a file to be compiled.
NOTE that run-class-uclcomp always prompts for a file.")

(defvar run-uclcomp-hook nil
  "This hook is run after a new Utah Common Lisp compiler process is created.")

(defun run-uclcomp (&optional program-name host-name)
  "Run a Utah Common Lisp compiler, with input and output via pipes through
buffer *UCL-Compiler*.  If a process is already running in buffer
*UCL-Compiler*, simply switch to that buffer.

If variable uclcomp-prompt-for-file is non-nil, this command prompts for a file
to compile and buffer *UCL-Compiler* does NOT become the default \"target\"
buffer.  If variable uclcomp-prompt-for-file is nil, then no prompt is offered
and buffer *UCL-Compiler* becomes the default \"target\" buffer.  See the
documentation for variable lispshell-target-buffer.

When called interactively, the environment variable UCL is examined to find the
program to run.  If UCL isn't set or $UCL/bin/uclcomp doesn't exist, use the
normal search path.

When called interactively, the process is created on the local machine.
Programs can use \"run-uclcomp\" to create processes on remote machines; in
this case, the buffer name includes the remote machine's name.

When a new process is created, the hook run-uclcomp-hook is executed."
  (interactive (list nil nil))
  (let ((process-connection-type nil)
	(old-lispshell-target-buffer (default-value 'lispshell-target-buffer))
	(source-file-args (lispshell-file-args uclcomp-prompt-for-file
					       "Compile Lisp file: ")))
    (lispshell-run (lispshell-find-executable program-name "UCL"
					      "/bin/uclcomp")
		   (cons "-i" (nconc (ucl-heapsize-args) (ucl-cpulimit-args)
				     source-file-args))
		   host-name "UCL-Compiler"
		   (function uclcomp-setup))
    (if source-file-args
	(setq-default lispshell-target-buffer old-lispshell-target-buffer))))

(defun run-class-uclcomp (&optional program-name host-name)
  "Run a class version of the Utah Common Lisp compiler, with input and output
via pipes through buffer *UCL-Compiler*.  If a process is already running in
buffer *UCL-Compiler*, simply switch to that buffer.

Buffer *UCL-Compiler* does NOT become the default \"target\" buffer; see the
documentation for variable lispshell-target-buffer.

When called interactively, the environment variable UCL is examined to find the
program to run.  If UCL isn't set or $UCL/bin/class-uclcomp doesn't exist, use
the normal search path.

When called interactively, the process is created on the local machine.
Programs can use \"run-class-uclcomp\" to create processes on remote machines;
in this case, the buffer name includes the remote machine's name.

When a new process is created, the hook run-uclcomp-hook is executed."
  (interactive (list nil nil))
  (let ((process-connection-type nil)
	(old-lispshell-target-buffer (default-value 'lispshell-target-buffer))
	(source-file-args (lispshell-file-args t "Compile Lisp file: ")))
    (lispshell-run (lispshell-find-executable program-name "UCL"
					      "/bin/class-uclcomp")
		   (nconc (ucl-heapsize-args) (ucl-cpulimit-args)
			  source-file-args)
		   host-name "UCL-Compiler"
		   (function uclcomp-setup))
    (setq-default lispshell-target-buffer old-lispshell-target-buffer)))

(defun uclcomp-setup ()
  (setq comint-prompt-regexp ucl-prompt-regexp)
  (setq lispshell-lisp-input-filter-regexp ucl-input-filter-regexp)
  (setq lispshell-send-eof-function (function ucl-send-eof))
  (setq lispshell-send-debugger-command-function
	(function ucl-send-debugger-command))
  (setq lispshell-load-command lispshell-common-lisp-load-command)
  (setq lispshell-compile-command lispshell-common-lisp-compile-command)
  (run-hooks 'run-uclcomp-hook))

;;;;
;;;; The functions for Utah Scheme (US), Utah Scheme with the compiler, and
;;;; Concurrent Utah Scheme.  Some of the information for these Lisps is
;;;; borrowed from the UCL setup.
;;;;

(defvar us-prompt-regexp "\\b[0-9]+ \\((break [0-9]+) \\)?\\[Scheme\\]-> "
  "The regexp for matching prompts from Utah Scheme.")

(defvar us-input-filter-regexp ucl-input-filter-regexp
  "The regexp for matching Utah Scheme debugger commands.")

(defvar uscomp-prompt-for-file t
  "*When non-nil, the command run-uscomp prompts for a file to be compiled.")

(defvar cus-nodes 0
  "*The number of processors (besides the initial processor) that Concurrent
Utah Scheme will use.")

(defvar run-us-hook nil
  "This hook is run after a new Utah Scheme process is created.")

(defvar run-uscomp-hook nil
  "This hook is run after a new Utah Scheme compiler process is created.")

(defvar run-cus-hook nil
  "This hook is run after a new Concurrent Utah Scheme process is created.")

(defun run-us (&optional program-name host-name)
  "Run a Utah Scheme process, with input and output via pipes through buffer
*US*.  If a process is already running in buffer *US*, simply switch to that
buffer.

Buffer *US* becomes the default \"target\" buffer; see the documentation for
variable lispshell-target-buffer.

When called interactively, the environment variable UCL is examined to find the
program to run.  If UCL isn't set or $UCL/bin/us doesn't exist, use the normal
search path.

When called interactively, the process is created on the local machine.
Programs can use \"run-us\" to create processes on remote machines; in this
case, the buffer name includes the remote machine's name.

When a new process is created, the hook run-us-hook is executed."
  (interactive (list nil nil))
  (let ((process-connection-type nil))
    (lispshell-run (lispshell-find-executable program-name "UCL" "/bin/us")
		   (cons "-i" (nconc (ucl-heapsize-args) (ucl-cpulimit-args)))
		   host-name "US"
		   (function (lambda () (us-setup 'run-us-hook))))))

(defun run-uscomp (&optional program-name host-name)
  "Run a Utah Scheme compiler, with input and output via pipes through buffer
*US-Compiler*.  If a process is already running in buffer *US-Compiler*, simply
switch to that buffer.

If variable uscomp-prompt-for-file is non-nil, this command prompts for a file
to compile and buffer *US-Compiler* does NOT become the default \"target\"
buffer.  If variable uscomp-prompt-for-file is nil, then no prompt is offered
and buffer *US-Compiler* becomes the default \"target\" buffer.  See the
documentation for variable lispshell-target-buffer.

When called interactively, the environment variable UCL is examined to find the
program to run.  If UCL isn't set or $UCL/bin/uscomp doesn't exist, use the
normal search path.

When called interactively, the process is created on the local machine.
Programs can use \"run-uscomp\" to create processes on remote machines; in this
case, the buffer name includes the remote machine's name.

When a new process is created, the hook run-uscomp-hook is executed."
  (interactive (list nil nil))
  (let ((process-connection-type nil)
	(old-lispshell-target-buffer (default-value 'lispshell-target-buffer))
	(source-file-args (lispshell-file-args uscomp-prompt-for-file
					       "Compile Scheme file: ")))
    (lispshell-run (lispshell-find-executable program-name "UCL" "/bin/uscomp")
		   (cons "-i" (nconc (ucl-heapsize-args) (ucl-cpulimit-args)
				     source-file-args))
		   host-name "US-Compiler"
		   (function (lambda () (us-setup 'run-uscomp-hook))))
    (if source-file-args
	(setq-default lispshell-target-buffer old-lispshell-target-buffer))))

(defun run-cus (&optional program-name host-name)
  "Run a Concurrent Utah Scheme process, with input and output via pipes
through buffer *CUS*.  If a process is already running in buffer *CUS*, simply
switch to that buffer.

Buffer *CUS* becomes the default \"target\" buffer; see the documentation for
variable lispshell-target-buffer.

When called interactively, the environment variable UCL is examined to find the
program to run.  If UCL isn't set or $UCL/bin/cus doesn't exist, use the normal
search path.

When called interactively, the root CUS process is created on the local
machine.  Programs can use \"run-cus\" to start CUS root processes on remote
machines; in this case, the buffer name includes the remote machine's name.

Concurrent Utah Scheme can run on more than one processor; the number of extra
processors is determined by the value of variable cus-nodes.  Use the command
lispshell-set-cus-nodes to change that value.

When a new process is created, the hook run-cus-hook is executed."
  (interactive (list nil nil))
  (let ((process-connection-type nil))
    (lispshell-run (lispshell-find-executable program-name "UCL" "/bin/cus")
		   (cons "-i" (nconc (ucl-heapsize-args)
				     (ucl-cpulimit-args) (cus-nodes-args)))
		   host-name "CUS"
		   (function (lambda () (us-setup 'run-cus-hook))))))

(defun cus-nodes-args ()
  (if (and (numberp cus-nodes) (> cus-nodes 0))
      (list "-n" (int-to-string cus-nodes))
    nil))

(defun us-setup (specific-us-hook)
  ;; This function is used to set up for all flavors of Utah Scheme.
  (setq comint-prompt-regexp us-prompt-regexp)
  (setq lispshell-lisp-input-filter-regexp us-input-filter-regexp)
  (setq lispshell-send-eof-function (function ucl-send-eof))
  (setq lispshell-send-debugger-command-function
	(function ucl-send-debugger-command))
  (setq lispshell-load-command lispshell-scheme-load-command)
  ;; US has a Common Lisp-like "compile-file" function.
  (setq lispshell-compile-command lispshell-common-lisp-compile-command)
  (run-hooks specific-us-hook))

(defun lispshell-set-cus-nodes (nodes)
  "Set the number of remote processors that Concurrent Utah Scheme will use."
  (interactive "NSet number of remote CUS nodes (processors) to: ")
  (if (>= nodes 0)
      (setq cus-nodes nodes)
    (error "Number of CUS nodes must be non-negative.")))

;;;;
;;;; The functions for Hewlett-Packard Common Lisp (HPCL).
;;;;

(defvar hpcl-prompt-regexp
  "\\b[0-9]+ \\(LISP\\|DEBUG ([0-9]+)\\) \\[[^]]*\\] > "
  "The regexp for matching prompts from HP Common Lisp.")

(defvar hpcl-input-filter-regexp "\\`\\s *\\(!.*\\)?\\s *\\'"
  "The regexp for matching HP Common Lisp debugger and inspector commands.")

(defvar hpcl-debugger-command-alist
  '((abort . "(sys::listener-abort)\n") (backtrace . "(debug::backtrace)\n")
    (continue . "(sys::listener-continue)\n")
    (quit . "(sys::listener-quit)\n"))
  "Association list of symbols and HP Common Lisp debugger command strings.")

(defvar run-hpcl-hook nil
  "This hook is run after a new HP Common Lisp process is created.")

(defun run-hpcl (&optional program-name host-name)
  "Run a Hewlett-Packard Common Lisp process with input and output through
buffer *HPCL*.  If a process is already running in buffer *HPCL*, simply switch
to that buffer.

Buffer *HPCL* becomes the default \"target\" buffer; see the documentation for
variable lispshell-target-buffer.

When called interactively, the environment variable LISP is examined to find
the program to run.  If LISP isn't set or $LISP/bin/cl doesn't exist, use the
normal search path.

When called interactively, the process is created on the local machine.
Programs can use \"run-hpcl\" to create processes on remote machines; in this
case, the buffer name includes the remote machine's name.

When a new process is created, the hook run-hpcl-hook is executed."
  (interactive (list nil nil))
  (let (;; (process-connection-type nil) makes HPCL go weird.
	)
    (lispshell-run (lispshell-find-executable program-name "LISP" "/bin/cl")
		   nil host-name "HPCL" (function hpcl-setup))))

(defun hpcl-setup ()
  (setq comint-prompt-regexp hpcl-prompt-regexp)
  (setq lispshell-lisp-input-filter-regexp hpcl-input-filter-regexp)
  (setq lispshell-send-eof-function (function hpcl-send-eof))
  (setq lispshell-send-debugger-command-function
	(function hpcl-send-debugger-command))
  (setq lispshell-load-command lispshell-common-lisp-load-command)
  (setq lispshell-compile-command lispshell-common-lisp-compile-command)
  (run-hooks 'run-hpcl-hook))

(defun hpcl-send-eof (process)
  ;; I'm not sure exactly what I should send.  For now, I tell the debugger
  ;; loop to quit.
  (comint-send-string process "!q\n"))

(defun hpcl-send-debugger-command (process command)
  (let ((command-string (cdr (assq command hpcl-debugger-command-alist))))
    (if command-string
	(comint-send-string process command-string)
      (error "No command string for \"%s\" has been defined." command))))

;;;;
;;;; Functions for running Portable Standard Lisp (PSL).
;;;;

(defvar psl-prompt-regexp "\\b[0-9]+ lisp\\( break ([0-9]+) \\)?> "
  "The regexp for matching prompts from Portable Standard Lisp.")

(defvar psl-input-filter-regexp "\\`\\s *\\w?\\s *\\'"
  "The regexp for matching Portable Standard Lisp debugger commands.")

(defvar psl-debugger-command-alist
  '((abort . "a\n") (backtrace . "i\n") (continue . "c\n") (quit . "q\n"))
  "Association list of symbols and Portable Standard Lisp debugger command
strings.")

(defvar run-psl-hook nil
  "This hook is run after a new Portable Standard Lisp process is created.")

(defun run-psl (&optional program-name host-name)
  "Run a Portable Standard Lisp process with input and output via pipes through
buffer *PSL*.  If a process is already running in buffer *PSL*, simply switch
to that buffer.

Buffer *PSL* becomes the default \"target\" buffer; see the documentation for
variable lispshell-target-buffer.

When called interactively, the environment variable psys is examined to find
the program to run.  If psys isn't set or $psys/psl doesn't exist, use the
normal search path.

When called interactively, the process is created on the local machine.
Programs can use \"run-psl\" to create processes on remote machines; in this
case, the buffer name includes the remote machine's name.

When a new process is created, the hook run-psl-hook is executed."
  (interactive (list nil nil))
  (let ((process-connection-type nil))
    (lispshell-run (lispshell-find-executable program-name "psys" "psl")
		   nil host-name "PSL" (function psl-setup))))

(defun psl-setup ()
  (setq comint-prompt-regexp psl-prompt-regexp)
  (setq lispshell-lisp-input-filter-regexp psl-input-filter-regexp)
  (setq lispshell-send-eof-function (function psl-send-eof))
  (setq lispshell-send-debugger-command-function
	(function psl-send-debugger-command))
  (setq lispshell-load-command lispshell-common-lisp-load-command)
  (setq lispshell-compile-command lispshell-common-lisp-compile-command)
  (run-hooks 'run-psl-hook))

(defun psl-send-eof (process)
  ;; I'm not exactly sure what I should send.  For now, I tell the debugger
  ;; loop to quit.
  (comint-send-string process "q\n"))

(defun psl-send-debugger-command (process command)
  (let ((command-string (cdr (assq command psl-debugger-command-alist))))
    (if command-string
	(comint-send-string process command-string)
      (error "No command string for \"%s\" has been defined." command))))

;;;;
;;;; Functions for running the Portable Common Lisp Subset (PCLS).  PCLS uses
;;;; some of the variables and functions defined for PSL.
;;;;

(defvar pcls-prompt-regexp
  "\\b[0-9]+ lisp \\(break ([0-9]+) \\)?\\[[^]]*\\] > "
  "The regexp for matching prompts from PCLS.")

(defvar run-pcls-hook nil
  "This hook is run after a new PCLS process is created.")

(defun run-pcls (&optional program-name host-name)
  "Run a PCLS process with input and output via pipes through buffer *PCLS*.
If a process is already running in buffer *PCLS*, simply switch to that buffer.

Buffer *PCLS* becomes the default \"target\" buffer; see the documentation for
variable lispshell-target-buffer.

When called interactively, the environment variable psys is examined to find
the program to run.  If psys isn't set or $psys/pcls doesn't exist, use the
normal search path.

When called interactively, the process is created on the local machine.
Programs can use \"run-pcls\" to create processes on remote machines; in this
case, the buffer name includes the remote machine's name.

When a new process is created, the hook run-pcls-hook is executed."
  (interactive (list nil nil))
  (let ((process-connection-type nil))
    (lispshell-run (lispshell-find-executable program-name "psys" "pcls")
		   nil host-name "PCLS" (function pcls-setup))))

(defun pcls-setup ()
  (setq comint-prompt-regexp pcls-prompt-regexp)
  (setq lispshell-lisp-input-filter-regexp psl-input-filter-regexp)
  (setq lispshell-send-eof-function (function psl-send-eof))
  (setq lispshell-send-debugger-command-function
	(function psl-send-debugger-command))
  (setq lispshell-load-command lispshell-common-lisp-load-command)
  (setq lispshell-compile-command lispshell-common-lisp-compile-command)
  (run-hooks 'run-pcls-hook))

;;;;
;;;; Functions for running a specific Lisp program.
;;;;

(defvar lispshell-generic-prompt-regexp "^[^>]*>+ *"
  "The regexp for matching prompts from a generic Lisp.")

(defvar run-generic-lisp-hook nil
  "This hook is run after a new generic Lisp process is created.")

(defun run-lisp (program-name &optional host-name)
  "Run a Lisp process with input and output through a buffer.

Run-lisp examines the program name to guess the type of the Lisp that will be
run.  For example, if you asked to run the program \"big-ucl\", run-lisp would
guess that \"big-ucl\" is a version of Utah Common Lisp, and would set things
appropriately for UCL.

If run-lisp doesn't recognize the program name, it sets up a generic Lisp
Shell in a buffer named *Lisp-Shell*.  If a process is already running in that
buffer, run-lisp simply switches to that buffer.  Buffer *Lisp-Shell* becomes
the default \"target\" buffer; see the documentation for variable
lispshell-target-buffer.

When called interactively, the process is created on the local machine.
Programs can use \"run-lisp\" to create processes on remote machines; in this
case, the buffer name includes the remote machine's name.

When a new generic Lisp process is created, the hook run-generic-lisp-hook
is executed."
  (interactive
   (list (let ((default-dir (car lispshell-prev-run-lisp-program))
	       (default-file (cdr lispshell-prev-run-lisp-program)))
	   (read-file-name (if default-file
			       (format "Run Lisp: (default %s) " default-file)
			     "Run Lisp: ")
			   default-dir
			   (concat default-dir default-file)
			   t))
	 nil))
  ;; Only substitute and expand when called interactively; this allows commands
  ;; like (run-lisp "ucl") to work as expected.  Maybe I should do this work in
  ;; the (interactive) declaration above...
  (if (interactive-p)
      (setq program-name (expand-file-name
			  (substitute-in-file-name program-name))))
  
  (let ((program-directory (file-name-directory program-name))
	(program-base-name (file-name-nondirectory program-name)))
    (if (interactive-p)
	(setq lispshell-prev-run-lisp-program
	      (cons program-directory program-base-name)))
    (cond ((string-match "class-uclcomp" program-base-name)
	   (run-class-uclcomp program-name host-name))
	  ((string-match "uclcomp" program-base-name)
	   (run-uclcomp program-name host-name))
	  ((string-match "ucl" program-base-name)
	   (run-ucl program-name host-name))
	  ((string-match "uscomp" program-base-name)
	   (run-uscomp program-name host-name))
	  ((string-match "cus" program-base-name)
	   (run-cus program-name host-name))
	  ((string-match "us" program-base-name)
	   (run-us program-name host-name))
	  ((string-match "psl" program-base-name)
	   (run-psl program-name host-name))
	  ((string-match "pcls" program-base-name)
	   (run-pcls program-name host-name))
	  ((string-match "cl" program-base-name)
	   (run-hpcl program-name host-name))
	  (t
	   (lispshell-run program-name nil host-name "Lisp-Shell"
			  (function lispshell-generic-setup))))))

(defun lispshell-generic-setup ()
  (setq comint-prompt-regexp lispshell-generic-prompt-regexp)
  (setq lispshell-remove-prompts nil)
  (setq lispshell-send-eof-function (function process-send-eof))
  (setq lispshell-send-debugger-command-function
	(function lispshell-generic-send-debugger-command))
  (run-hooks 'run-generic-lisp-hook))

(defun lispshell-generic-send-debugger-command (process command)
  (error "No command string for \"%s\" has been defined." command))

;;;;
;;;; Functions for running a specific Lisp program on a remote machine.
;;;;

(defun run-remote-lisp (host-name)
  "Run a Lisp process on a remote machine with input and output through a
buffer.  This command prompts for the names of the remote machine and Lisp
program, and then calls the function run-lisp.  See the documentation for
function run-lisp for more information."
  (interactive
   (list (let ((host-name
		(read-string (format "Run remote Lisp on host: (default %s) "
				     lispshell-prev-run-remote-lisp-host))))
	   (if (string-equal host-name "")
	       lispshell-prev-run-remote-lisp-host
	     host-name))))
  
  ;; Get the name of the program to run.  This code is copied almost verbatim
  ;; from run-lisp, above.
  (let* ((program-name
	  (let ((default-dir (car lispshell-prev-run-lisp-program))
		(default-file (cdr lispshell-prev-run-lisp-program)))
	    (read-file-name (if default-file
				(format "Run remote Lisp: (default %s) "
					default-file)
			      "Run remote Lisp: ")
			    default-dir
			    (concat default-dir default-file)
			    ;; Don't require an exact match; the remote file
			    ;; system may not look like ours.
			    nil))
	  )
	 ;; Unlike run-lisp, we don't expand or substitute in the file name.
	 (program-directory (file-name-directory program-name))
	 (program-base-name (file-name-nondirectory program-name)))
    
    (setq lispshell-prev-run-lisp-program
	  (cons program-directory program-base-name))
    (setq lispshell-prev-run-remote-lisp-host host-name)
    
    (run-lisp program-name host-name)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Finally, run the load hook.
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(run-hooks 'lispshell-load-hook)

;; End of file.

