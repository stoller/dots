(load "mh-e")

;; To get mh-letter-mode-map defined.
(if (>= emacs-major-version 19)
  (load "mh-comp.el"))

;; Metamail
(autoload 'metamail "metamail-buffer" nil t)

;;; Usual hook.

(defun my-mail-mode-hook ()
  (set-fill-column 74)
  (turn-on-auto-fill))

(setq mail-mode-hook 'my-mail-mode-hook)
(setq mh-letter-mode-hook 'my-mail-mode-hook)

(setq mh-reply-default-reply-to "to")

;;; Clean the headers, leaving only important fields. I just hate X- fields!

(setq mh-clean-message-header t)
(setq mh-visible-headers
      "^From: \\|^To: \\|^Subject: \\|^Date: \\|^Cc: \\|^Content-Type: ")

(put 'backup-inhibited 'permanent-local t)

;;; PGP

(autoload 'mc-install-write-mode "mailcrypt" nil t)
(autoload 'mc-install-read-mode "mailcrypt" nil t)
(add-hook 'mail-mode-hook 'mc-install-write-mode)

(add-hook 'mh-folder-mode-hook 'mc-install-read-mode)
(add-hook 'mh-letter-mode-hook 'mc-install-write-mode)
(add-hook 'mh-show-hook        'mc-mh-decrypt-message)

(setq mc-passwd-timeout 600000)
(setq mc-pgp-always-sign 'never)
(setq mc-always-replace t)

;;; The default size is way too small.
(if (> (screen-height) 35)
  (setq mh-summary-height 20)
  (setq mh-summary-height 6))

(setq *my-send* (concat (getenv "HOME") "/bin/machine/mysend"))

;;; When I compose a new message, I like the cursor to be at the first
;;; field I need to fill in.

(defun my-compose-hook (to subject cc)
  (make-local-variable 'backup-inhibited)
  (setq backup-inhibited t)
  (set-buffer-modified-p t)		; Make sure buffer is written
  (save-buffer)
  (set-file-modes buffer-file-name 384)
  (goto-char (point-min))
  (if (equal to "")
    (re-search-forward "^To: $" nil t)
    (if (equal subject "")
      (re-search-forward "^Subject: $" nil t)
      (progn
	(re-search-forward "^------" nil t)
	(next-line 1)))))

(setq mh-compose-letter-function 'my-compose-hook)


;;; Indent region over by a tab stop, and put a > at the beginning of each
;;; line.

(defun over ()
  (interactive)
  (if (null (mark))
    (error "Must set mark.")
    (save-excursion
      (if (> (point) (mark))
	(exchange-point-and-mark))
      (save-restriction
	(narrow-to-region (point) (mark))
	(replace-regexp "^" "> ")))))


;; Do not use rmail on any machine other than snowball.

(defun maybe-do-rmail ()
  (interactive)
  (if (or (equal (system-name) "fast.cs.utah.edu")
	  (equal (system-name) "marker.cs.utah.edu"))
    (progn
      (mh-rmail))
    (error "No RMAIL here")))

;; My own version of the send function that does not ask me anything.

(defun my-smail ()
  "Compose and send mail with the MH mail system, using a new frame."
  (interactive)
  (mh-find-path)
  (mh-send "" "" ""))

;; Somthing to put the current configuraton back to the way it was when I first
;; started the draft message. I use this when I have switched buffers around
;; while editing my draft, and the original message I am repying to has been
;; buried.

(defun mh-my-config ()
  (interactive)
  (let ((msg mh-sent-from-msg))
    (switch-to-buffer mh-sent-from-folder)
    (mh-show msg)
    (switch-to-buffer mh-show-buffer)
    (delete-other-windows)
    (pop-to-buffer (get-buffer "draft"))))

(define-key mh-letter-mode-map "\C-c\C-t" 'mh-my-config)


;; Truely bogus. Since I use slocal, and because it detaches from the
;; controlling tty, mail that goes to myself hangs up because emacs releases
;; the tty. So, use my own send routine that hangs on long enough to deliver
;; local mail.

(defun mh-send-letter (&optional arg)
  (interactive "P")
  (run-hooks 'mh-before-send-letter-hook)
  (set-buffer-modified-p t)		; Make sure buffer is written
  (save-buffer)
  (message "Sending...")
  (let ((draft-buffer (current-buffer))
	(file-name (buffer-file-name))
	(config mh-previous-window-config))
    (save-excursion
      (set-buffer (get-buffer-create " *mh-temp*"))
      (erase-buffer))
    (let ((process (apply 'start-process
			  "send" nil *my-send*
			  (list "-nodraftfolder" "-noverbose" file-name))))
      (set-process-filter process 'mh-process-daemon))
    (cond ((or (not arg)
	       (y-or-n-p "Kill draft buffer? "))
	   (kill-buffer draft-buffer)
	   (if config
	       (set-window-configuration config))))
    (run-hooks 'mh-after-send-letter-hook)
    (message "Sending...done")))

;; Yank the current message into the draft buffer, but prepend some headers
;; to it. The default is to yank either no headers, or all the headers.
;; Neither one is acceptable to me.

(setq mh-yank-from-start-of-msg 'body)

(defun my-mh-yank-cur-msg ()
  (interactive)
  (let ((buffer  (current-buffer))
	(from    nil)
	(subject nil))
    (set-buffer mh-sent-from-folder)
    (set-buffer mh-show-buffer)
    (save-excursion
      (setq from    (mh-get-field "From:"))
      (setq subject (mh-get-field "Subject:")))
    ;; Emacs 19 bug. mark-active can be t even when no mark set.
    (if (not (mark t))
      (setq mark-active nil))
    (set-buffer buffer)
    (mh-yank-cur-msg)
    (let ((to-point (mark)))
      (save-excursion
	(narrow-to-region to-point to-point)
	(insert "From: " from "\n")
	(insert "Subject: " subject "\n\n")
	(mh-insert-prefix-string mh-ins-buf-prefix)
	(widen)))))

(define-key mh-letter-mode-map "\C-c\C-y" 'my-mh-yank-cur-msg)
