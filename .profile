#
# Shell startup file. If using bash, source that file as well.
#

if [ -f ~/.bashrc ]; then
	source ~/.bashrc 
fi

#--------------------------------------------------------------------------- 
# Terminal set-up.
#
function termset {
	set -o noglob
	eval `tset -Q -s -e\ -k\ -m "switch=19200:vt100" \
		-m "dialup:?vt100" -m "switch:?vt100" \
		-m "network<=2400:?vt100" -m "network=19200:vt100" \
		-m "network:?vt100" $1`
	set +o noglob
}

if [ "$TERM" != "" ]; then
	termset $TERM
else
	termset
fi

case $TERM in
	xterm)
		stty -page
		;;

	xterm-256color)
		stty erase 
		;;

	vt220)
		setenv TERMCAP :li#30:tc=vt100:
		stty erase 
		;;

	vt200 | vt52 | t10)
		stty erase 
		;;

	vt100)
		stty erase 
		reset
		;;
esac
echo Terminal type is $TERM

#--------------------------------------------------------------------------- 
# See who is here.
#
echo ""
echo "-- Current users: " ; finger

#--------------------------------------------------------------------------- 
# What kind of machine,
#
echo ""
echo "-- Machine info: " ; uname -a

#--------------------------------------------------------------------------- 
# Set my HOME env variable to match the actual NFS /home/css mountpoint.
# This causes emacs to print ~/ instead of /a/home/css/stoller.
#
if [ "$HOME" = "/home/css/stoller" ]; then
	if [ "$hostname" = "fast" ]; then
		export HOME=/remote/users/staff/stoller
	else
		export HOME=/a/home/css/stoller
	fi
fi

#--------------------------------------------------------------------------- 
# Execute local machine init file if it exists.
#
if [ -f ~/.machinit/$hostname ]; then
	echo ""
	echo "-- Sourcing ~/.machinit/$hostname"
	source ~/.machinit/$hostname
fi

#echo ""
#echo "-- Sourcing ~/.todo"
#cat ~/.todo

#--------------------------------------------------------------------------- 
# Execute miscellaneous commands:
#
mesg y

